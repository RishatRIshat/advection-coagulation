#ifdef CUDA_ADV
#include "transport.h"
namespace advection
{
	__host__ __device__ double sigma(double const &r, double const &dr, int const &PML, int const & size)
	{
		if (r>(dr*(size-PML))) return pow((r-(size-PML)*dr)/(PML*dr),2)*3.0*log(10.0)*10.0/(PML*dr);
		else return 0;
	}

	__global__ void  cuda_solver_res(double *d_res, double *d_u, double *d_vel, double dt, int batch, int size, double dr)
	{
		int numThreads = blockDim.x * blockDim.y * gridDim.x * gridDim.y;
		int global_id = (threadIdx.y + blockIdx.y * blockDim.y)*blockDim.x*gridDim.x + (threadIdx.x + blockIdx.x * blockDim.x);
		int ix = global_id % size;
		int iy = (global_id - ix) / size;
		for(int i = global_id; i < size * batch;)
		{
			if ((ix !=0) && (ix != (size-1)))
			{
				double a;
				double b = (d_u[i+1] - d_u[i-1]);
				if (( d_u[i+1]-d_u[i])*(d_u[i]-d_u[i-1]) > 0) a = min(min((abs(b)) / (2.0 * dr), 2.0 * abs(d_u[i + 1] - d_u[i]) / dr), 
													2.0 * abs(d_u[i] - d_u[i-1]) / dr) * (b >= 0 ? 1.0: -1.0);
				else a = 0;
				d_res[i] = d_u[i]+dr/2.0*(1.0-d_vel[iy]*dt/dr)*a;
			}
			i+= numThreads;
			ix = i % size;
			iy = (i - ix) / size;
		}
	}

	__global__ void cuda_solver_u(double *d_res, double *d_u, double *d_vel, double dt, int batch, int size, double dr, int PML)
	{
		int numThreads = blockDim.x * blockDim.y * gridDim.x * gridDim.y;
		int global_id = (threadIdx.y + blockIdx.y * blockDim.y)*blockDim.x*gridDim.x + (threadIdx.x + blockIdx.x * blockDim.x);
		int ix = global_id % size;
		int iy = (global_id - ix) / size;
		
		for(int i = global_id; i < size * batch;)
		{
			if ((ix !=0) && (ix != (size-1)))
			{
				d_u[i] = d_vel[iy] * ((d_res[i] - d_res[i-1]) / dr + sigma(dr*ix, dr, PML, size)*d_u[i]);
			}
			i+= numThreads;
			ix = i % size;
			iy = (i - ix) / size;
		}
	}

	void advection_1d_cuda::solver(double *&values, double const &dt, double *&coefs, int const & batch)
	{
		double * d_res;
		cudaMalloc((void **) &d_res, size*batch*sizeof(double));
		cudaMemcpy(d_res, values, size*batch*sizeof(double), cudaMemcpyDeviceToDevice); 
		
		dim3 block(8, 8);
		dim3 mesh((size + block.x - 1)/block.x, (batch + block.y - 1)/block.y);	
	
		cuda_solver_res<<<mesh, block>>>(d_res, values, coefs, dt, batch, size, dr);
		getLastCudaError("error1");
		//cudaDeviceSynchronize();
		cuda_solver_u<<<mesh, block>>>(d_res, values, coefs, dt, batch, size, dr, PML);
		getLastCudaError("error2");
	
		cudaFree(d_res);
	}	
}
#endif

