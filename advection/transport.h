#pragma once

#ifdef MPI_ADV
#include <mpi.h>
#endif

#ifdef CUDA_ADV
#include <cuda_runtime.h>
#include <cufft.h>
#include <math.h>
#include "../cuda_utils/helper_cuda.h"
#include <cusolverSp.h>
#endif

#include <stdio.h>
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include "../fade2d/Fade_2D.h"

using namespace GEOM_FADE2D;
using namespace std;

namespace advection
{
#ifndef CUDA_ADV
	double sigma(double const &r, double const &dr, int const &PML, int const & size);
#endif
#ifdef CUDA_ADV
	__host__ __device__ double sigma(double const &r, double const &dr, int const &PML, int const & size);
#endif
	class advection_1d
	{
	protected:
		int size;
		double dr;
		int PML;
	public:
		advection_1d(double dr, int PML, int size) : dr(dr), PML(PML), size(size) {}
		virtual void solver(double*& values, double const& dt, double*& coefs, int const& batch = 1);
	};

#ifdef CUDA_ADV
	class advection_1d_cuda : public advection_1d
	{
	public:
		advection_1d_cuda(double dr, int PML, int size) : advection_1d(dr, PML, size) {};
		virtual void solver(double*& values, double const& dt, double*& coefs, int const& batch = 1) override;
	};
#endif

#ifdef MPI_ADV
	class advection_1d_mpi : public advection_1d
	{
	protected:
		int rank;
		int p;
		int N_per_core;
	public:
		advection_1d_mpi(double dr, int PML, int size, int rank, int p, int N_per_core) : advection_1d(dr, PML, size), rank(rank), p(p), N_per_core(N_per_core) {}
		virtual void solver(double*& values, double const& dt, double*& coefs, const int& batch = 1) override;
	};
#endif

	class advection_2d
	{
	protected:
		int size;
		Vector2 * face_normals;
		Vector2 * barycenter_dist_vector;
		double * tr_areas;
		double * xbarys;
		int * neighbor_ids;
		double * inverse_weights;
		double limiter(double const& r_factor, double const& weight);
		void find_gradients(Vector2 * values, double * input);
		void calc_dynamic_arrays(vector<Triangle2*> triangles);
	public:
		bool * is_boundary;
		bool * is_no_slip;
		bool * is_source;	
		advection_2d(vector<Triangle2*> triangles) : size(triangles.size()) 
		{
			calc_dynamic_arrays(triangles);
		}
		~advection_2d();
		advection_2d(const advection_2d&) = delete;
		advection_2d& operator= (const advection_2d&) = delete;
		advection_2d& operator= (const advection_2d&&) = delete;
		advection_2d(const advection_2d&&) = delete;
		virtual void solver(double*& values, double const& dt, Vector2*& coefs, const int& batch = 1);
	};

#ifdef MPI_ADV
	class advection_2d_mpi : public advection_2d
	{
	protected:
		int rank;
		int x_dim;
		int y_dim;

		double left_boundary;
		double right_boundary;
		double upper_boundary;
		double downward_boundary;
		
		int left_size;
		int right_size;
		int up_size;
		int down_size;

		bool* send_left;
		bool* send_right;
		bool* send_up;
		bool* send_down;

		int * from_up_received_ids;
		int * from_down_received_ids;
		int * from_right_received_ids;
		int * from_left_received_ids;


		void calc_mpi_arrays(vector<Triangle2*> gl_triangles, vector<Triangle2*> local_triangles);
	public:
		int * global_ids_reference;
		advection_2d_mpi(int rank, int x_dim, int y_dim, double left, double right, double up, double down, vector<Triangle2*> gl_triangles, vector<Triangle2*> local_triangles) : advection_2d(local_triangles), rank(rank), x_dim(x_dim), y_dim(y_dim), left_boundary(left), right_boundary(right), upper_boundary(up), downward_boundary(down)
		{
			calc_mpi_arrays(gl_triangles, local_triangles);
		}
		virtual void solver(double *& values, double const& dt, Vector2*& coefs, const int& batch = 1);
	};
#endif
}
