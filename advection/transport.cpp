#include "transport.h"

namespace advection
{
#ifndef CUDA_ADV
	double sigma(double const &r, double const &dr, int const &PML, int const & size)
	{
		if (r>(dr*(size-PML))) return pow((r-(size-PML)*dr)/(PML*dr),2)*3.0*log(10.0)*10.0/(PML*dr);
		else return 0;
	}
#endif
	double advection_2d::limiter(double const &r_factor, double const &weight)
	{
		//return max(0.0, min(weight*r_factor,min(0.5*(1.0+r_factor), weight)));//MC
		//return (r_factor*r_factor+r_factor)/(r_factor*r_factor+1.0);
		//return weight*r_factor/(r_factor*r_factor+1.0);
		//return max(0.0, min(weight*r_factor, min(weight, (1.0+r_factor)/2.0)));
		//return (0.5*weight*r_factor+0.5*weight*fabs(r_factor))/(weight-1+fabs(r_factor));
		//return 1.0;
		return max(0.0, min(1.0, r_factor));//minmod
		//double temp_result = max(min(r_factor, weight), min(1.0, weight*r_factor));//superbee	
		//return max(temp_result, 0.0);//superbee
	}

	void advection_1d::solver(double *&values, double const &dt, double *&coefs, int const &batch)
	{
		double a,b;
	double ** res = new double * [size];
		for (int i = 0; i < size; i++) res[i] = new double [batch];
		for (int m=0;m<batch;m++)
		{
			res[0][m]= values[0+m*size];
			res[size-1][m]= values[size-1+m*size];
		}
		for (int i=1;i<size-1;i++){
			for (int m=0;m<batch;m++) {
				b = (values[i+1+m*size]-values[i-1+m*size]);
				if ((values[i+1+m*size]-values[i+m*size])*(values[i+m*size]-values[i-1+m*size])>0) a=min(min(abs(b)/(2.0*dr),2.0*abs(values[i+1+m*size]-values[i+m*size])/dr),2.0*abs(values[i+m*size]-values[i-1+m*size])/dr)*(b>=0 ? 1.0:-1.0);
				else a=0;
				res[i][m] = values[i+m*size]+dr/2.0*(1.0-coefs[m]*dt/dr)*a;
			}
		}
		for (int i=1;i<size-1;i++){
			for(int m=0;m<batch;m++) values[i+m*size] = coefs[m]*((res[i][m]-res[i-1][m])/dr+sigma(dr*i, dr, PML, size)*values[i+m*size]);
		}
		for (int i = 0; i < size; i++) delete [] res[i];
		delete [] res;

	}

#ifdef MPI_ADV
	void advection_1d_mpi::solver(double *&values, double const & dt, double *&coefs, const int & batch)
	{
		
		double a,b;
		double ** res = new double * [N_per_core];
		for (int i = 0; i < N_per_core; i++) res[i] = new double [batch];
			
		if (rank==0){
				for (int m = 0; m < batch; m++){
					res[1][m] = values[1*batch+m];
			}
		} else if (rank==p-1){
				for (int m = 0; m < batch; m++)
				{
					res[N_per_core-2][m] = values[(N_per_core - 2)*batch+m];
				}
		}
		int left;
		int right;
		if (rank != 0) left = 1;
		else left = 0;
		if (rank != p-1) right = 1;
		else right = 0;
		MPI_Request request[2];
		MPI_Status status;
		//U EXCHANGE HERE
		if (left) MPI_Isend(&values[1*batch], batch, MPI_DOUBLE, rank - 1 , rank, MPI_COMM_WORLD, &request[1]);
		if (right) MPI_Isend(&values[(N_per_core-2)*batch], batch, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &request[2]);

		if (left) MPI_Recv(values, batch, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD, &status);
		if (right) {MPI_Recv(&values[(N_per_core-1)*batch], batch, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &status);}

		//U EXCHANGE HERE END

			for (int i = (rank==0 ? 2 : 1); i < ((rank==p-1) ? N_per_core-2 : N_per_core - 1); i++){
				for (int m = 0; m < batch; m++){
						b = (values[(i + 1)*batch+m] - values[(i - 1)*batch+m]);
						if (( (values[(i + 1)*batch+m]-values[i*batch+m])*(values[i*batch+m]-values[(i-1)*batch+m]))>0)
							a = min(min(abs(b) / (2.0 * dr),
										2.0 * abs(values[(i + 1)*batch+m] - values[i*batch+m]) / dr),
									2.0 * abs(values[i*batch+m] - values[(i-1)*batch+m]) / dr) * (b >= 0 ? 1.0: -1.0);
						else a = 0;
						res[i][m] = values[i*batch+m]+dr/2.0*(1.0-coefs[m]*dt/dr)*a;
				}
			}
		if (left) MPI_Wait(&request[1], &status);
		if (right) MPI_Wait(&request[2], &status);
		//RES EXCHANGE HERE
		if (right) MPI_Isend(res[N_per_core-2], batch, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &request[1]);
		if (left)  MPI_Recv(res[0], batch, MPI_DOUBLE, rank - 1, rank-1, MPI_COMM_WORLD, &status);
		//RES EXCHANGE HERE

			for (int i = (rank==0 ? 2 : 1);i < N_per_core - 1; i++){
				for(int m = 0;m < batch; m++)
						values[i*batch+m] = (coefs[m] * res[i][m] - coefs[m] * res[i - 1][m]) / dr + coefs[m] * sigma(dr*(i+rank*(N_per_core-2)), dr, PML, size) * values[i*batch+m];
			}
		if (right) MPI_Wait(&request[1], &status);
			for (int i = 0; i < N_per_core; i++) delete [] res[i];
			delete [] res;
	}
#endif

	void advection_2d::calc_dynamic_arrays(vector<Triangle2*> triangles)
	{
		ofstream barys;
		barys.open("cellcenters.txt");
		xbarys = new double [size];
		is_boundary = new bool [size];
		neighbor_ids = new int [3*size];
		face_normals = new Vector2 [3*size];
		barycenter_dist_vector = new Vector2 [3*size];
		tr_areas = new double [size];
		inverse_weights = new double [3*size];
		is_no_slip = new bool [size];
		is_source = new bool [size];
		int k = 0;
		double leftmost_x = triangles[0]->getCorner(0)->x();
		double rightmost_x = leftmost_x;
		double bottom_y = triangles[0]->getCorner(0)->y();
		double top_y = bottom_y;
		int counter = 0;
		for (auto it(triangles.begin()); it != triangles.end(); it++)
		{
			barys << -(*it)->getBarycenter().y() << " " << (*it)->getBarycenter().x() << " " << 0.0 << endl;
			xbarys[counter] = (*it)->getBarycenter().x();
			for (int i = 0; i < 3; i++)
			{
				double value = (*it)->getCorner(i)->x();
				if (value > rightmost_x) rightmost_x = value;
				else if (value < leftmost_x) leftmost_x = value;
				double value_y = (*it)->getCorner(i)->y();
				if (value_y < bottom_y) bottom_y = value_y;
				else if (value_y > top_y) top_y = value_y;
			}
			counter++;
		}
		barys.close();
		for (auto it(triangles.begin()); it != triangles.end(); it++)
		{
			is_boundary[k] = false;
			is_no_slip[k] = false;
			is_source[k] = false;
			for (int i = 0; i < 3; i++)
			{
				int index = k*3+i;
				auto opposite_tr = (*it)->getOppositeTriangle(i);
				auto neighbor = find(triangles.begin(), triangles.end(), opposite_tr);
				if ((opposite_tr == nullptr) || (neighbor == triangles.end()))
				{
					for (int j=0; j< 3; j++)
					{
						if (((*it)->getCorner(j)->x() == leftmost_x) || ((*it)->getCorner(j)->x() == rightmost_x ) || (*it)->getCorner(j)->x() <= 0.5)
						{
							is_boundary[k] = true;
							break;
						}
						else if ((*it)->getCorner(j)->y() == bottom_y)
						{
							is_no_slip[k] = true;
							break;
						}
					}
					neighbor_ids[index] = -1;
				}
				else neighbor_ids[index] = distance(triangles.begin(), neighbor);			
				double radius = 0.05*rightmost_x/4.0;	
				if ( pow( (*it)->getCorner(i)->x(),2) + pow( (*it)->getCorner(i)->y()-top_y ,2) <= pow(radius, 2) )
				{
					is_source[k] = true;
				}
			}
			k++;
		}	
		k = 0;
		for (auto it(triangles.begin()); it != triangles.end(); it++)
		{
			Point2 barry = (*it)->getBarycenter();
			for (int i = 0; i < 3; i++)
			{
				int index = k*3+i;
				
				Point2* corner1 = (*it)->getCorner((i+1)%3);
				Point2* corner2 = (*it)->getCorner((i+2)%3);
				Point2 center = Point2((corner1->x() + corner2->x())/2.0, (corner1->y() + corner2->y())/2.0);
				double length = pow(pow(corner1->x()-corner2->x(), 2) + pow(corner1->y() - corner2->y(), 2), 0.5);
				double x1, y1;
				double x2, y2;
				if (fabs(corner1->y()-corner2->y())<=1e-6)
				{
					x1 = x2 = center.x();
					y1 = center.y() + length;
					y2 = center.y() - length;
				}
				else if (fabs(corner1->x()-corner2->x())<=1e-6)
				{
					y1 = y2 = center.y();
					x1 = center.x() + length;
					x2 = center.x() - length;
				}
				else
				{
					double n_x = (corner1->y()-corner2->y())/(corner2->x()-corner1->x());
					y1 = center.y() + pow(length*length/(1.0 + n_x*n_x),0.5);
					y2 = center.y() - pow(length*length/(1.0 + n_x*n_x),0.5);
					x1 = center.x() + n_x * (y1 - center.y());
					x2 = center.x() + n_x * (y2 - center.y());
				}
				Vector2 dist1 = Vector2(x1 - barry.x(), y1 - barry.y());
				Vector2 dist2 = Vector2(x2 - barry.x(), y2 - barry.y()); 
				Point2 dest = (dist2.length() > dist1.length()) ? Point2(x2, y2) : Point2(x1, y1);
				face_normals[index] = Vector2(dest.x()-center.x(), dest.y()-center.y());
				if (neighbor_ids[index] != -1)
				{
					Point2 neighborbary = (*it)->getOppositeTriangle(i)->getBarycenter();
					inverse_weights[index] = (pow(pow(barry.x()-center.x(),2)+pow(barry.y()-center.y(),2),0.5) + pow(pow(neighborbary.x()-center.x(),2)+pow(neighborbary.y()-center.y(),2),0.5))/pow(pow(barry.x()-center.x(),2)+pow(barry.y()-center.y(),2),0.5);
					//assert(fabs(inverse_weights[index]-2.0) < 1e-3);
				}
				else
				{
					inverse_weights[index] = 0.0;
				}
			}
			k++;
		}
		k = 0;
		for (auto it(triangles.begin()); it != triangles.end(); it++)
		{
			for (int i = 0; i < 3; i++)
			{
				int index = k*3+i;
				if (neighbor_ids[index] != -1)
				{
					Point2 origin = (*it)->getBarycenter();
					Point2 destination = (*it)->getOppositeTriangle(i)->getBarycenter();
					double bary_x, bary_y;
					if (fabs(bary_x = destination.x()-origin.x()) <= 1e-6) bary_x = 0.0;
					if (fabs(bary_y = destination.y()-origin.y()) <= 1e-6) bary_y = 0.0;
					barycenter_dist_vector[index] = Vector2(bary_x, bary_y);
				}
				else barycenter_dist_vector[index] = Vector2(0.0, 0.0);
			}
			k++;
		}
		k = 0;
		for (auto it(triangles.begin()); it != triangles.end(); it++)
		{
			tr_areas[k] = (*it)->getArea2D();
			k++;
		}
	}

	advection_2d::~advection_2d()
	{
		delete [] face_normals;
		delete [] barycenter_dist_vector;
		delete [] neighbor_ids;
		delete [] tr_areas;
		delete [] xbarys;
		delete [] inverse_weights;
		delete [] is_no_slip;
		delete [] is_source;
	}

	void advection_2d::find_gradients(Vector2 * values, double * input)
	{
		for (int j = 0; j < size; j++)
		{
			if (!is_boundary[j])
			{	
				double A = 0.0;
				double B = 0.0;
				double C = 0.0;
				double D = 0.0;
				double E = 0.0;
				double F = 0.0;
				for (int k = 0; k < 3; k++)
				{
					if (neighbor_ids[j*3+k] != -1)
					{
						A += pow(barycenter_dist_vector[j*3+k].x()*1.0/barycenter_dist_vector[j*3+k].length(), 2);
						B += barycenter_dist_vector[j*3+k].x()*barycenter_dist_vector[j*3+k].y()*pow(1.0/barycenter_dist_vector[j*3+k].length(), 2);
						D += pow(barycenter_dist_vector[j*3+k].y()*1.0/barycenter_dist_vector[j*3+k].length(), 2);		
						E += barycenter_dist_vector[j*3+k].x()*pow(1.0/barycenter_dist_vector[j*3+k].length(), 2)*(input[neighbor_ids[j*3+k]]-input[j]);
						F += barycenter_dist_vector[j*3+k].y()*pow(1.0/barycenter_dist_vector[j*3+k].length(), 2)*(input[neighbor_ids[j*3+k]]-input[j]);
					}
				}
				C = B;
				values[j] = Vector2(D/(A*D-B*C)*E - B/(A*D-B*C)*F, - C/(A*D-B*C)*E + A/(A*D-B*C)*F);
			}
			else values[j] = Vector2(0.0, 0.0);
		}
	}

	void advection_2d::solver(double *&u, double const &dt, Vector2 *&velocities, int const &batch)
	{
		for (int i = 0; i < batch; i++)
		{
			double phi_u_star;

			double * face_values;
			face_values = new double [size * 3];
			Vector2 * cell_gradients;
			cell_gradients = new Vector2 [size];
			double * r_factor_face;
			r_factor_face = new double [size *3];
			Vector2 * interpolated_velocities;
			interpolated_velocities = new Vector2 [size * 3];

			for (int j = 0; j < size; j++)
			{
				int value_index = j+size*i;
				if (!is_boundary[j])
				{	
					for (int k = 0; k < 3; k++)
					{
						if (neighbor_ids[j*3+k] != -1)
						{
							//if (((is_no_slip[j]) && (face_normals[j*3+k]*velocities[j]>=0.0))||((is_no_slip[neighbor_ids[j*3+k]])&&(face_normals[j*3+k]*velocities[j]<=0.0))) interpolated_velocities[j*3+k] = Vector2(0.0, 0.0);
							//else interpolated_velocities[j*3+k] = velocities[neighbor_ids[j*3+k]+size*i]/inverse_weights[j*3+k] + velocities[value_index]/(inverse_weights[j*3+k]/(inverse_weights[j*3+k]-1.0));
							interpolated_velocities[j*3+k] = velocities[neighbor_ids[j*3+k]+size*i]/inverse_weights[j*3+k] + velocities[value_index]/(inverse_weights[j*3+k]/(inverse_weights[j*3+k]-1.0));
						}
						else
						{
							interpolated_velocities[j*3+k] = Vector2(0.0, 0.0);
						}
					}
				}
			}
			find_gradients(cell_gradients, &u[size*i]);
			for (int j = 0; j < size; j++)
			{
				int value_index = j+size*i;
				if (!is_boundary[j])
				{
					for (int k = 0; k < 3; k++)
					{
						double phi_u_star;
						if (neighbor_ids[j*3+k] == -1) face_values[j*3+k] = 0.0;
						else if (face_normals[j*3+k]*interpolated_velocities[j*3+k]<0.0)
						{
							phi_u_star = u[value_index] - 2.0 * (cell_gradients[neighbor_ids[j*3+k]]) * (-barycenter_dist_vector[j*3+k]);
							if (phi_u_star < 0.0) {phi_u_star = 0.0;}
							else if (phi_u_star > 1.0) phi_u_star = 1.0;
							if (fabs(u[value_index] - u[neighbor_ids[j*3+k]+size*i]) > 1e-5)
							{
								r_factor_face[j*3+k] = (u[neighbor_ids[j*3+k]+size*i] - phi_u_star)/(u[value_index]-u[neighbor_ids[j*3+k]+size*i]);
							}
							else
							{
								if (fabs(u[neighbor_ids[j*3+k]+size*i] - phi_u_star) <= 1e-5) r_factor_face[j*3+k] = ((u[neighbor_ids[j*3+k]+size*i] - phi_u_star) >= 0.0) ? 1.0 : -1.0;
								else 
								{
									r_factor_face[j*3+k] = (u[neighbor_ids[j*3+k]+size*i] - phi_u_star) / 1e-5;
								}
							}
							face_values[j*3+k] = u[neighbor_ids[j*3+k]+size*i] + limiter(r_factor_face[j*3+k], inverse_weights[j*3+k]/(inverse_weights[j*3+k]-1.0)) / (inverse_weights[j*3+k]/(inverse_weights[j*3+k]-1.0)) * (u[value_index] - u[neighbor_ids[j*3+k]+size*i]); 
						}
						else
						{
							phi_u_star = u[neighbor_ids[j*3+k]+size*i] - 2.0 * cell_gradients[j] * barycenter_dist_vector[j*3+k];
							if (phi_u_star < 0.0) {phi_u_star = 0.0;}
							else if (phi_u_star > 1.0) phi_u_star = 1.0;
							if (fabs(u[value_index] - u[neighbor_ids[j*3+k]+size*i]) > 1e-5)
							{
								r_factor_face[j*3+k] = (u[value_index] - phi_u_star)/(u[neighbor_ids[j*3+k]+size*i]-u[value_index]);
							}
							else
							{
								if (fabs(u[value_index] - phi_u_star)<= 1e-5) r_factor_face[j*3+k] = ((u[value_index] - phi_u_star) >= 0.0) ? 1.0 : -1.0;
								else r_factor_face[j*3+k] = (u[value_index] - phi_u_star) / 1e-5;
							}
							face_values[j*3+k] = u[value_index] + limiter(r_factor_face[j*3+k], inverse_weights[j*3+k]) / inverse_weights[j*3+k] * (u[neighbor_ids[j*3+k]+size*i] - u[value_index]);
						}
						if (isnan(face_values[j*3+k]))
						{
							throw runtime_error("isnan(face_values[j*3+k]) from solver");
						}
					}
				}
			}
			for (int j = 0; j < size; j++)
			{
				int value_index = j+size*i;
				if (!is_boundary[j] && !is_no_slip[j])
				{
					double temp = 0.0;
					for (int k = 0; k < 3; k++)
					{
						temp += face_values[k+j*3] * interpolated_velocities[j*3+k] * face_normals[j*3+k]; 
					}
					//u[value_index] = u[value_index] - dt * (temp / tr_areas[j] + sigma(xbarys[j], 0.01, 15, 300)*u[value_index]);
					//u[value_index] = (temp / tr_areas[j] + sigma(xbarys[j], 0.02, 5, 75)*u[value_index]);					
					//u[value_index] = u[value_index] - dt * temp / tr_areas[j];
					u[value_index] = temp / tr_areas[j];
				}
			}
			delete [] interpolated_velocities;
			delete [] face_values;
			delete [] cell_gradients;
			delete [] r_factor_face;
		}
	}

#ifdef MPI_ADV
	void advection_2d_mpi::solver(double *&u, double const &dt, Vector2 *&velocities, int const &batch)
	{
		//cout << "entered solver, rank " << rank << endl; 
		vector<double> send_data_left;
		vector<double> send_data_right;
		vector<double> send_data_up;
		vector<double> send_data_down;

		double * receive_data_left = new double [3*left_size*batch];
		double * receive_data_right = new double [3*right_size*batch];
		double * receive_data_up = new double [3*up_size*batch];
		double * receive_data_down = new double [3*down_size*batch];
		for ( int i = 0; i < batch; i++)
		{
			for (int k = 0; k < size; k++)
			{
				int index = k+i*size;
				if (send_left[k])
				{
					send_data_left.push_back(u[index]);
					send_data_left.push_back(velocities[index].x());
					send_data_left.push_back(velocities[index].y());
				}
				if (send_right[k]) 
				{
					send_data_right.push_back(u[index]);
					send_data_right.push_back(velocities[index].x());
					send_data_right.push_back(velocities[index].y());
				}
				if (send_up[k])
				{
					send_data_up.push_back(u[index]);
					send_data_up.push_back(velocities[index].x());
					send_data_up.push_back(velocities[index].y());
				}
				if (send_down[k])
				{
					send_data_down.push_back(u[index]);
					send_data_down.push_back(velocities[index].x());
					send_data_down.push_back(velocities[index].y());
				}
			}
		}
		MPI_Request request[4];
		//U EXCHANGE HERE
		double * left = send_data_left.data();
		double * right = send_data_right.data();
		double * up = send_data_up.data();
		double * down = send_data_down.data();
		//if (!send_data_left.empty()) cout << "not empyt left " << rank <<endl;
		//if (!send_data_right.empty()) cout << "not empyt r " << rank <<endl;
		//if (!send_data_down.empty()) cout << "not empyt d " << rank <<endl;
		//if (!send_data_up.empty()) cout << "not empyt u " << rank <<endl;
		if (left_size!=0) MPI_Isend(left, send_data_left.size(), MPI_DOUBLE, rank - 1 , rank, MPI_COMM_WORLD, &request[0]);
		if (right_size!=0) MPI_Isend(right, send_data_right.size(), MPI_DOUBLE, rank + 1 , rank, MPI_COMM_WORLD, &request[1]);
		if (up_size!=0) MPI_Isend(up, send_data_up.size(), MPI_DOUBLE, rank + x_dim, rank, MPI_COMM_WORLD, &request[2]);
		if (down_size!=0) MPI_Isend(down, send_data_down.size(), MPI_DOUBLE, rank - x_dim , rank, MPI_COMM_WORLD, &request[3]);

		assert((left_size!=0)==(!send_data_left.empty()));
		assert((right_size!=0)==(!send_data_right.empty()));
		assert((up_size!=0)==(!send_data_up.empty()));
		assert((down_size!=0)==(!send_data_down.empty()));

		if (!send_data_left.empty()) MPI_Recv(receive_data_left, left_size*3*batch, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (!send_data_right.empty()) MPI_Recv(receive_data_right, right_size*3*batch, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (!send_data_up.empty()) MPI_Recv(receive_data_up, up_size*3*batch, MPI_DOUBLE, rank + x_dim, rank + x_dim, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (!send_data_down.empty()) MPI_Recv(receive_data_down, down_size*3*batch, MPI_DOUBLE, rank - x_dim, rank - x_dim, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		//MPI_Status status;
		if (left_size!=0) MPI_Wait(&request[0], MPI_STATUS_IGNORE);
		if (right_size!=0) MPI_Wait(&request[1], MPI_STATUS_IGNORE);
		if (up_size!=0) MPI_Wait(&request[2], MPI_STATUS_IGNORE);
		if (down_size!=0) MPI_Wait(&request[3], MPI_STATUS_IGNORE);
		//U EXCHANGE HERE END
		if (!send_data_left.empty())
		{
			for (int i = 0; i < batch; i ++)
			{
				for (int k = 0; k < left_size; k++)
				{
					u[from_left_received_ids[k]+i*size] = receive_data_left[3*k+i*left_size*3];
					velocities[from_left_received_ids[k]+i*size] = Vector2(receive_data_left[3*k+1+i*left_size*3], receive_data_left[3*k+2+i*left_size*3]);
				}
			}
		}
		if (!send_data_right.empty())
		{
			for (int i = 0; i < batch; i ++)
			{
				for (int k = 0; k < right_size; k++)
				{
					u[from_right_received_ids[k]+i*size] = receive_data_right[3*k+i*right_size*3];
					velocities[from_right_received_ids[k]+i*size] = Vector2(receive_data_right[3*k+1+i*right_size*3], receive_data_right[3*k+2+i*right_size*3]);
				}
			}
		}
		if (!send_data_up.empty())
		{
			for (int i = 0; i < batch; i ++)
			{
				for (int k = 0; k < up_size; k++)
				{
					u[from_up_received_ids[k]+i*size] = receive_data_up[3*k+i*up_size*3];
					velocities[from_up_received_ids[k]+i*size] = Vector2(receive_data_up[3*k+1+i*up_size*3], receive_data_up[3*k+2+i*up_size*3]);
				}
			}
		}
		if (!send_data_down.empty())
		{
			for (int i = 0; i < batch; i ++)
			{
				for (int k = 0; k < down_size; k++)
				{
					u[from_down_received_ids[k]+i*size] = receive_data_down[3*k+i*down_size*3];
					velocities[from_down_received_ids[k]+i*size] = Vector2(receive_data_down[3*k+1+i*down_size*3], receive_data_down[3*k+2+i*down_size*3]);
				}
			}
		}
		advection_2d::solver(u, dt, velocities, batch);
		//cout << "finished solver, rank " << rank << endl;
	}

	void advection_2d_mpi::calc_mpi_arrays(vector<Triangle2*> gl_triangles, vector<Triangle2*> local_triangles)
	{
		left_size = 0;
		right_size = 0;
		down_size = 0;
		up_size = 0;
		bool right_bound, left_bound, up_bound, down_bound;
		if (rank < x_dim)
		{
			down_bound = true;
		}
		if (rank % x_dim == 0)
		{
			left_bound = true;
		}
		if (rank % x_dim == x_dim-1)
		{
			right_bound = true;
		}
		if (x_dim*y_dim - rank <= x_dim)
		{
			up_bound = true;
		}
		send_left = new bool [size];
		send_right = new bool [size];
		send_up = new bool [size];
		send_down = new bool [size];

		global_ids_reference = new int [size];

		vector<int> send_data_left;
		vector<int> send_data_right;
		vector<int> send_data_up;
		vector<int> send_data_down;
		int k = 0;
		for (auto it(local_triangles.begin()); it != local_triangles.end(); it++)
		{
			auto global_tr = find(gl_triangles.begin(), gl_triangles.end(), *it);

			//if (global_tr == gl_triangles.end()) cout << "it happened" << endl;
			//else cout << "it didn't happen" << endl;
			global_ids_reference[k] = distance(gl_triangles.begin(), global_tr);	
			//if (rank ==0) cout << right_boundary << " " << left_boundary <<  " " << upper_boundary << " " << downward_boundary << endl;
			bool is_to_send_left = false;
			bool is_to_send_right = false;
			bool is_to_send_up = false;
			bool is_to_send_down = false;
			bool is_to_receive = false;
			int count_l = 0, count_r = 0, count_d = 0, count_u = 0;
			for (int i = 0; i < 3; i++)
			{
				double x_val = (*it)->getCorner(i)->x();
				double y_val = (*it)->getCorner(i)->y();
				if (x_val == right_boundary/* && (y_val != upper_boundary && y_val != downward_boundary)*/)
				{
					if ((*it)->getBarycenter().x()<right_boundary)
					{
						count_r++;
					}
					else is_to_receive = true;
				}
				if (x_val == left_boundary /*&& (y_val != upper_boundary && y_val != downward_boundary)*/)
				{
					if ((*it)->getBarycenter().x()>left_boundary)
					{
						count_l++;
					}
					else is_to_receive = true;
				}
				if (y_val == upper_boundary/* && (x_val != left_boundary && x_val != right_boundary)*/)
				{
					if ((*it)->getBarycenter().y()<upper_boundary)
					{
						count_u++;
					}
					else is_to_receive = true;
				}
				if (y_val == downward_boundary/* && (x_val != left_boundary && x_val != right_boundary)*/)
				{					
					if ((*it)->getBarycenter().y()>downward_boundary)
					{
						count_d++;
					}
					else is_to_receive = true;
				}
			}
			if (count_r ==2) is_to_send_right = true;
			if (count_l ==2) is_to_send_left = true;
			if (count_d ==2) is_to_send_down = true;
			if (count_u==2) is_to_send_up = true;
				


			if (is_to_send_left && !left_bound)
			{
				send_left[k] = true;
				send_data_left.push_back(global_ids_reference[k]);
				left_size++;
			}			
			else send_left[k] = false;
			if (is_to_send_right && !right_bound)
			{
				send_right[k] = true;
				send_data_right.push_back(global_ids_reference[k]);
				right_size++;
			}
			else send_right[k] = false;
			if (is_to_send_up && !up_bound)
			{
				send_up[k] = true;
				send_data_up.push_back(global_ids_reference[k]);
				up_size++;
			}
			else send_up[k] = false;
			if (is_to_send_down && !down_bound)
			{
				send_down[k] = true;
				send_data_down.push_back(global_ids_reference[k]);
				down_size++;
			}
			else send_down[k] = false;

			if (is_to_receive) is_boundary[k] = true;
			k++;
		}
		from_left_received_ids = new int [left_size];
		from_right_received_ids = new int [right_size];
		from_up_received_ids = new int [up_size];
		from_down_received_ids = new int [down_size];

		MPI_Request request[4];
		MPI_Status status[4];
		int * send_ids_left = send_data_left.data();
		int * send_ids_right = send_data_right.data();
		int * send_ids_up = send_data_up.data();
		int * send_ids_down = send_data_down.data();
		assert(send_data_left.size()==left_size);
		assert(send_data_down.size()==down_size);
		assert(send_data_up.size()==up_size);
		assert(send_data_right.size()==right_size);
		if (left_size !=0) MPI_Isend(send_ids_left, left_size, MPI_INT, rank - 1 , rank, MPI_COMM_WORLD, &request[1]);
		if (right_size !=0) MPI_Isend(send_ids_right, right_size, MPI_INT, rank + 1 , rank, MPI_COMM_WORLD, &request[2]);
		if (up_size !=0) MPI_Isend(send_ids_up, up_size, MPI_INT, rank + x_dim , rank, MPI_COMM_WORLD, &request[3]);
		if (down_size !=0) MPI_Isend(send_ids_down, down_size, MPI_INT, rank - x_dim , rank, MPI_COMM_WORLD, &request[0]);

		//if (rank ==0) cout << "left rank0 " << left_size << endl;
		//if (rank ==0) cout << "right rank0 " << right_size << endl;
		//if (rank ==1) cout << "left rank1 " << left_size << endl;
		//if (rank ==1) cout << "right rank1 " << right_size << endl;
		//if (rank==2) cout << " left rank2 " << left_size << endl;
		//if (rank ==2) cout << "right rank2 " << right_size << endl;
		//if (rank ==3) cout << " left rank3 " << left_size << endl;
		//if (rank ==3) cout << "right rank3 " << right_size << endl;
	
		int * buffer_left = new int [left_size];
		int * buffer_right = new int [right_size];
		int * buffer_up = new int [up_size];
		int * buffer_down = new int [down_size];
		if (!send_data_left.empty()) MPI_Recv(buffer_left, left_size, MPI_INT, rank - 1, rank - 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (!send_data_right.empty()) MPI_Recv(buffer_right, right_size, MPI_INT, rank + 1, rank + 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (!send_data_up.empty()) MPI_Recv(buffer_up, up_size, MPI_INT, rank + x_dim, rank + x_dim, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (!send_data_down.empty()) MPI_Recv(buffer_down, down_size, MPI_INT, rank - x_dim, rank - x_dim, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		for (int j = 0; j < size; j++)
		{
			for (int i = 0; i < left_size; i++)
			{
				if(global_ids_reference[j] == buffer_left[i]) from_left_received_ids[i] = j;
			}
			for (int i = 0; i < right_size; i++)
			{
				if(global_ids_reference[j] == buffer_right[i]) from_right_received_ids[i] = j;
			}
			for (int i = 0; i < up_size; i++)
			{
				if(global_ids_reference[j] == buffer_up[i]) from_up_received_ids[i] = j;
			}
			for (int i = 0; i < down_size; i++)
			{
				if(global_ids_reference[j] == buffer_down[i]) from_down_received_ids[i] = j;
			}
		}
		delete [] buffer_left;
		delete [] buffer_right;
		delete [] buffer_up;
		delete [] buffer_down;
	}
#endif
}
