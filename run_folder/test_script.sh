#!/bin/bash -l
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --ntasks-per-node=1
##SBATCH --gres=gpu:1
##SBATCH --time=0-0:1:0
#SBATCH -p cpu_big
##SBATCH --switches=1@10:00:00

#module load mpi/hpcx-v2.3.0-cuda-for_OFED-4.5-1.0.1.0
module load compilers/fftw-3.3.8
module load gpu/cuda-9.2
module load compilers/intel_2019.5.281 
module load compilers/gcc-7.3.0
module load mpi/openmpi-3.1.4
mpirun 2D_MPI_CPU.exe down
#mpirun 2D_MPI_CUDA.exe coag_out
