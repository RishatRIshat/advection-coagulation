import matplotlib as m
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.special import kn
from scipy.special import kvp

cdict = {
  'red'  :  ( (0.0, 0.25, .25), (0.02, .59, .59), (1., 1., 1.)),
  'green':  ( (0.0, 0.0, 0.0), (0.02, .45, .45), (1., .97, .97)),
  'blue' :  ( (0.0, 1.0, 1.0), (0.02, .75, .75), (1., 0.45, 0.45))
}

cm = m.colors.LinearSegmentedColormap('my_colormap', cdict, 1024)

non_flux = np.loadtxt("kernel_nonflux.txt")
non_flux = np.flipud(non_flux)
flux_first = np.loadtxt("kernel_flux_first.txt")
flux_first = np.flipud(flux_first)
flux_second = np.loadtxt("kernel_flux_second.txt")
flux_second = np.flipud(flux_second)
flux_third = np.loadtxt("kernel_flux.txt")
flux_third = np.flipud(flux_third)
print(flux_first.shape)
print(non_flux.shape)

N = non_flux.shape[0]
x = np.zeros(N)
y = np.zeros(N)
resdif = np.zeros([N,N])
for i in range(N):
    for j in range(N):
        resdif[i][j] = abs(flux_third[i][j]-non_flux[i][j])
        
def do_plot(n, resy, title):
    #plt.clf()
    plt.subplot(1, 5, n)
    plt.xticks(x, " ")
    plt.yticks(y, " ")
    print(np.amin(resy))
    print(np.amax(resy))
    plt.pcolor(resy, cmap=cm, vmin=np.amin(resy), vmax=np.amax(resy))
    plt.title(title)
    plt.colorbar()

plt.figure()
do_plot(1, flux_first, "flux kernel, v0 = 1.0")
do_plot(2, flux_second, "flux kernel, v0 = 0.1")
do_plot(3, flux_third, "flux kernel, v0 = 0.00001")
do_plot(4, non_flux, "non flux kernel")
do_plot(5, resdif, "abs(v0 = 0.00001, non flux)")
plt.show()

