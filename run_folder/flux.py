import matplotlib as m
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.special import kn
from scipy.special import kvp

cdict = {
  'red'  :  ( (0.0, 0.25, .25), (0.02, .59, .59), (1., 1., 1.)),
  'green':  ( (0.0, 0.0, 0.0), (0.02, .45, .45), (1., .97, .97)),
  'blue' :  ( (0.0, 1.0, 1.0), (0.02, .75, .75), (1., 0.45, 0.45))
}

cm = m.colors.LinearSegmentedColormap('my_colormap', cdict, 1024)

N = 20
dr = 0.01
v0 = 1.0
max_iter = 10
coef = 0.0001
x = np.empty(N)
y = np.empty(N)
for i in range(N):
    x[i] = (i+1)*dr
    y[i] = (i+1)*dr

res = np.empty([N,N])
for i in range(N):
    for j in range(N):
        res[N-1-i][j] = 4*100*(coef/x[i]+coef/y[j])*(x[i]+y[j])
        #res[i][j] = 4*math.pi*(coef+coef)*(x[i]+y[j])#diffusion coef is constant

res2 = np.zeros([N,N])
for i in range(N):
    for j in range(N):
        print(i, j)
        for k in range(N):
            res2[N-1-i][j] += kvp(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))/kn(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))
            #res2[i][j] += kvp(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))/kn(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))
        res2[N-1-i][j] *= -(x[i]+y[j])**2 * v0
res3 = np.zeros([N,N])
v0 = 0.1
for i in range(N):
    for j in range(N):
        for k in range(N):
            res3[N-1-i][j] += kvp(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))/kn(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))
            #res3[i][j] += kvp(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))/kn(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))
        res3[N-1-i][j] *= -(x[i]+y[j])**2 * v0
        #res3[i][j] = res2[i][j]-res[i][j]

res4 = np.zeros([N,N])
v0 = 0.01
for i in range(N):
    for j in range(N):
        for k in range(N):
            res4[N-1-i][j] += kvp(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))/kn(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))
            #res3[i][j] += kvp(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))/kn(k, v0/(2*(coef/x[i] + coef/y[j]))*(x[i]+y[j]))
        res4[N-1-i][j] *= -(x[i]+y[j])**2 * v0
        #res3[i][j] = res2[i][j]-res[i][j]

resdif = np.zeros([N,N])
for i in range(N):
    for j in range(N):
        resdif[i][j] = res4[i][j]-res[i][j]

def do_plot(n, resy, title):
    #plt.clf()
    plt.subplot(1, 5, n)
    plt.xticks(x, " ")
    plt.yticks(y, " ")
    print(np.amin(resy))
    print(np.amax(resy))
    plt.pcolor(resy, cmap=cm, vmin=np.amin(resy), vmax=np.amax(resy))
    plt.title(title)
    plt.colorbar()

plt.figure()
do_plot(1, res2, "flux kernel v0 = 1.0")
do_plot(2, res3, "flux kernel v0 = 0.1")
do_plot(3, res4, "flux kernel v0 = 0.01")
do_plot(4, res, "non flux kernel")
do_plot(5, resdif, "difference between v0=0.01 and non flux")
plt.show()
