import matplotlib.pyplot as plt
import csv
import numpy as np
from scipy.interpolate import interp1d

#######################################
#example.txt:
#0.0, 0.0
#0.25, 1.92
#0.5, 3.84
#0.75, 4.8
#1.0, 5
#example2.txt:
#0.0, 0.0
#0.25, 0.92
#0.5, 2.84
#0.75, 3.8
#1.0, 4
#example3.txt
#0.0, 0.0
#0.25, 0.92
#0.5, 1.84
#0.75, 2.8
#1.0, 3
########################################


x = []
y = []

with open('example.txt','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        x.append(float(row[0]))
        y.append(float(row[1]))

f=interp1d(x,y, kind='cubic')

x2 = []
y2 = []

with open('example2.txt','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        x2.append(float(row[0]))
        y2.append(float(row[1]))

f2=interp1d(x2,y2, kind='cubic')

x3 = []
y3 = []

with open('example3.txt','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        x3.append(float(row[0]))
        y3.append(float(row[1]))

f3=interp1d(x3,y3, kind='cubic')



ynew = []
xnew = np.linspace(0.0, 1.0)
cons = []
cons2 = []
cons3 = []

for i in range(50):
    ynew.append(float(xnew[i]/0.1))
    cons.append(5.01)
    cons2.append(4.01)
    cons3.append(3.01)
ynew = np.array(ynew)
print(xnew.shape)
print(ynew.shape)
#plt.plot(xnew, ynew, label='H/c_y(i=1)')
plt.plot(xnew, cons, label='L=5.01', linestyle=':')
plt.plot(xnew, cons2, label='L=4.01', linestyle=':')
plt.plot(xnew, cons3, label='L=3.01', linestyle=':')
plt.ylim([0, 10])
plt.plot(xnew, f(xnew),  label='L(H) non flux const')
plt.plot(xnew, f2(xnew),  label='L(H) flux const')
plt.plot(xnew, f3(xnew),  label='L(H) flux periodic')
plt.xlabel('H - River depth')
plt.ylabel('L - Lengh of contaminated part of the river')
plt.title('River pollution')
plt.legend()
plt.show()
