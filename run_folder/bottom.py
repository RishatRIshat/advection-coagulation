import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import numpy as np
import csv

x = []
y = []
z = []

with open('bottom_last_frame.txt') as csvfile:
    reader = csv.DictReader(csvfile,delimiter=' ')
    for data in reader:
        x.append(2*float(data['X']))
        y.append(2*float(data['Y']))
        z.append(float(data['Z']))

x = np.array(x)
y = np.array(y)
z = np.array(z)
val = int(len(x)/10)
arryinds = y.argsort()
y = y[arryinds[::-1]]
z = z[arryinds[::-1]]
x = x[arryinds[::-1]]
for i in range(val):
    arryinds = x[10*i:10*(i+1)].argsort()
    y[10*i:10*(i+1)] = y[10*i:10*(i+1)][arryinds[::-1]]
    z[10*i:10*(i+1)] = z[10*i:10*(i+1)][arryinds[::-1]]
    x[10*i:10*(i+1)] = x[10*i:10*(i+1)][arryinds[::-1]]

x = x.reshape((val, 10))
y = y.reshape((val, 10))
z = z.reshape((val, 10))

x1 = []
y1 = []

x1.append(x[0])
y1.append(y[0])

for i in range(1, val-1):
    
    x1.append(x[i])
    y1.append(y[i])
x1.append(x[val-1])
y1.append(y[val-1])

x = np.array(x1)
y = np.array(y1)

for _ in range(100):
    z1 = []
    z1.append(z[0])

    for i in range(1, val-1):
        z1.append(0.5*z[i-1]+0.5*z[i+1])
    z1.append(z[val-1])
    z = np.array(z1)


#print(y)
#print(x)
#print(y.shape)
#print(x.shape)
#print(z.shape)

z = z[:-1, :-1]
levels = MaxNLocator(nbins=15).tick_values(z.min(), z.max())

cmap = plt.get_cmap('PiYG')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

fig, ax0 = plt.subplots()

cf = ax0.contourf(y[:-1, :-1], x[:-1, :-1], z, cmap=cmap)
fig.colorbar(cf, ax=ax0)
ax0.set_title('contourf with levels')
plt.show()
