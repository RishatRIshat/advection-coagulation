#pragma once
#include <stdio.h>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <cstring>
#include <iostream>
#include <fstream>
#include "coagulation/tensor_train.h"
#include <vector>
#include "fade2d/Fade_2D.h"

#ifdef CUDA_FFT
#include <cuda_runtime.h>
#endif
using namespace std;
using namespace GEOM_FADE2D;
namespace wrappers
{
	void advection_init_smooth(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord);
	void velocity_init_potential(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord);
	void advection_init_setuha(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord);
	void advection_init_setuha_full(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord);
	void advection_periodic(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord, int t, bool * is_source);

#ifdef CUDA_FFT
	__device__ __host__ double K(const int & u, const int &v, const double h);
	__global__ void new_time_step(const double *init, const double *smol, double *result, const int N, const int S, const double dt);
	__global__ void equalize_data(double *init, double *smol, double *result, const int N, const int S);
	__global__ void new_time_step_aligned(const double *init, const double *smol, double *result, const int N, const int S, const double dt);
	__global__ void equalize_data_aligned(double *init, double *smol, double *result, const int N, const int S);
	__global__ void calc_smoluch(const double *L1, const double *L2, const double *n_k, double * result, const int N);
	__global__ void calc_L2_correction(const double * n_k, const double h, double * result, const int N);
#endif

#ifndef CUDA_FFT
	double K(const int & u, const int &v, const double h);
	double K_flux(const int & u, const int &v, const double h);
	double K_nonflux(const int & u, const int &v, const double h);
#endif
	class TKernel: public TMatrix
	{
		public:
               	double h;
               	TKernel (const int &m, const int &n) : TMatrix(m , n) {}
		double value (const int &i, const int &j) override {return h*K(i,j,h);}
	};

	TCross_Parallel_v1 default_crossed_kernel(const double & tolerance, const int & size, const double & dm);
}

