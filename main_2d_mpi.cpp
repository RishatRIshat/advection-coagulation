#include <algorithm>
#include <memory>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <iomanip>
#include "fade2d/Fade_2D.h"
#include <cassert>
#include <utility>
#include "mesh/example_meshes.h"
#include "coagulation/tensor_train.h"
#include "advection/transport.h"
#include "velocity/velocity.h"
#include <string>
#include "wrappers.h"
using namespace GEOM_FADE2D;
using namespace std;
using namespace example_meshes;
using namespace advection;
using namespace wrappers;
int main(int argc, char ** argv)
{
	MPI_Init(&argc, &argv);
	int size_of_group;
    	int rank;
	MPI_Comm comm = MPI_COMM_WORLD;
    	MPI_Comm_size(comm, &size_of_group);
    	MPI_Comm_rank(comm, &rank);
        omp_set_num_threads(1);
	if (rank==0) cout << "mpi 2d version" << endl;


	//*********************TO*REMOVE*EVAPORATION*SET*THIS*TO*ZERO**************
	double K_evap = 0.7;
	//*************************************************************************
	
	double h, dr, dt;
	int TIME, max_particle_size, TOTAL_FRAMES;
	double length_x, length_y;
	double nominal_velocity_y, init_velocity_x;

	double volume_fraction, gravity, particle_density, fluid_density, fluid_viscosity;

	if(argc !=2) {if(rank==0) cout << "need filename" << endl; return 2;}

	string filename{argv[1]};
	ifstream arguments;
	arguments.open(filename);
	vector<string> data;
	string line;
	int i = 0;
	while(getline(arguments, line))
	{
		data.push_back(line);
		i++;
	}
	if (data.size() != 14) {if(rank==0) cout << "wrong data" << endl; return 2;}
	h = stod(data[0]);//0.05
	dr = stod(data[1]);//0.04
	dt = stod(data[2]);//0.001
	TIME = stoi(data[3]);//1
	max_particle_size = stoi(data[4]);//50
	TOTAL_FRAMES = stoi(data[5]);//100
	length_x = stod(data[6]);//2.0
	length_y = stod(data[7]);//0.5
	init_velocity_x = stod(data[8]);

	volume_fraction = stod(data[9]);
	gravity = stod(data[10]);
	particle_density = stod(data[11]);
	fluid_density = stod(data[12]);
	fluid_viscosity = stod(data[13]);
	
	//***********************************TEST KERNEL BEGIN***********************
	if (rank == 0)
	{
		std::ofstream kernel;
		kernel.open("kernel_nonflux.txt");
		for (int i = 0; i < 50; i ++)
		{
			for (int j = 0; j < 50; j++) kernel << wrappers::K_nonflux(i, j, h/10.0) << " ";
			kernel << std::endl;
		}
		kernel.close();
		kernel.open("kernel_flux.txt");	
		for (int i = 0; i < 50; i ++)
		{
			for (int j = 0; j < 50; j++) kernel << wrappers::K(i, j, h/10.0) << " ";
			kernel << std::endl;
		}
		kernel.close();
	}
	//***********************************TEST KERNEL END*************************




	nominal_velocity_y = 2.0*gravity/(9.0*fluid_viscosity)*(volume_fraction*particle_density - fluid_density)*pow((1.0/volume_fraction), 2.0/3.0)*0.005*0.005;
	if(rank==0) cout << "nominal downward velocity is "<< nominal_velocity_y << endl;
	bool if_paint = TOTAL_FRAMES ==0 ? false : true;
	int periods = TOTAL_FRAMES==0 ? TIME: TIME/TOTAL_FRAMES;//constexprs?
	double max_face_length = dr+0.25*dr;
	double min_face_length = dr-0.25*dr;

	vector<Point2*> vertices;
	vector<Triangle2*> visual;
	Fade_2D GRID;
	auto steps = mesh_structured_rect_var_mpi(GRID, visual, vertices, min_face_length, max_face_length, length_x, length_y, size_of_group);
	
	int x = length_x/steps[0];
	int y = length_y/steps[1];
	if (rank==0) cout << "X = " << x << endl << "Y = " << y << endl;


	int *ids[size_of_group];
	if (rank == 0)
	{
	for (int i = 0; i < size_of_group; i++)
	{
		int coord_x = i%x;
		int coord_y = (i - coord_x)/x;
		vector<Triangle2*> visual_core;
		for (auto it(visual.begin()); it != visual.end(); it++)
		{
			int counter = 0;
			for (int i = 0; i < 3; i++)
			{
				double val_x = (*it)->getCorner(i)->x();
				double val_y = (*it)->getCorner(i)->y();
				if (((val_x == steps[0]*(coord_x+1) || val_x == steps[0]*(coord_x)) && (val_y <= steps[1]*(coord_y+1) && val_y >= steps[1]*(coord_y))) || (val_y == steps[1]*(coord_y+1) || val_y == steps[1]*(coord_y)) && (val_x <= steps[0]*(coord_x+1) && val_x >= steps[0]*(coord_x))) counter++;
			}
			if(counter==2) visual_core.push_back(*it);
			else if ((*it)->getBarycenter().x() < steps[0]*(coord_x+1) && (*it)->getBarycenter().x() > steps[0]*(coord_x) && (*it)->getBarycenter().y() < steps[1]*(coord_y+1) && (*it)->getBarycenter().y() > steps[1]*(coord_y)) visual_core.push_back(*it);
		}
		ids[i] = new int [visual_core.size()];
		int k = 0;
		for (auto it(visual_core.begin()); it !=visual_core.end(); it++)
		{
			auto global_tr = find(visual.begin(), visual.end(), *it);
			ids[i][k] = distance(visual.begin(), global_tr);
			k++;
		}
		//string name = "zone_visualizer_core_" + std::to_string(i) + ".ps";
		//cout << name;
		//Visualizer2 visualizer_core(name);
		//for (auto it(visual_core.begin()); it != visual_core.end(); it++)
		//{
		//	visualizer_core.addObject(**it, Color(1,0,0,0.02, true));
		//}
		//visualizer_core.writeFile();
	}
	}


	int coord_x = rank%x;
	int coord_y = (rank - coord_x)/x;
	vector<Triangle2*> visual_core;
	for (auto it(visual.begin()); it != visual.end(); it++)
	{
		int counter = 0;
		for (int i = 0; i < 3; i++)
		{
			double val_x = (*it)->getCorner(i)->x();
			double val_y = (*it)->getCorner(i)->y();
			if (((val_x == steps[0]*(coord_x+1) || val_x == steps[0]*(coord_x)) && (val_y <= steps[1]*(coord_y+1) && val_y >= steps[1]*(coord_y))) || (val_y == steps[1]*(coord_y+1) || val_y == steps[1]*(coord_y)) && (val_x <= steps[0]*(coord_x+1) && val_x >= steps[0]*(coord_x))) counter++;
		}
		if(counter==2) visual_core.push_back(*it);
		else if ((*it)->getBarycenter().x() < steps[0]*(coord_x+1) && (*it)->getBarycenter().x() > steps[0]*(coord_x) && (*it)->getBarycenter().y() < steps[1]*(coord_y+1) && (*it)->getBarycenter().y() > steps[1]*(coord_y)) visual_core.push_back(*it);
	}

	double tolerance = 1e-4;    			
    	TCross_Parallel_v1 crossed_kernel = default_crossed_kernel(tolerance, max_particle_size, h);		

	//FFTW START
	int R_value = crossed_kernel.get_rank();
	int V_value = crossed_kernel.get_columns_number();
#ifdef FFTW
	fftw_complex *ub = (fftw_complex *) fftw_malloc(R_value * V_value * sizeof(fftw_complex));
      	fftw_complex *vb = (fftw_complex *) fftw_malloc(R_value * V_value * sizeof(fftw_complex));
	fftw_plan * plan_v = (fftw_plan *) fftw_malloc(R_value * sizeof(fftw_plan));
	fftw_plan * plan_u = (fftw_plan *) fftw_malloc(R_value * sizeof(fftw_plan));
	fftw_plan * plan_inverse = (fftw_plan *) fftw_malloc(R_value * sizeof(fftw_plan));
	for (int i = 0; i < R_value; i++)
	{
		plan_v[i] = fftw_plan_dft_1d(max_particle_size, vb+i*max_particle_size, vb+i*max_particle_size, FFTW_FORWARD, FFTW_ESTIMATE);
		plan_u[i] = fftw_plan_dft_1d(max_particle_size, ub+i*max_particle_size, ub+i*max_particle_size, FFTW_FORWARD, FFTW_ESTIMATE);
		plan_inverse[i] = fftw_plan_dft_1d(max_particle_size, ub+i*max_particle_size, ub+i*max_particle_size, FFTW_BACKWARD, FFTW_ESTIMATE);	
	}
	//FFTW END
#endif
#ifdef MKL_FFT
	VSLConvTaskPtr * tasks = new VSLConvTaskPtr [R_value];
	vsldConvNewTask1D(tasks, VSL_CONV_MODE_AUTO, V_value, V_value, V_value);
	for (int r = 1; r < R_value; r++)
	{
		vslConvCopyTask(tasks + r, tasks[0]);
	}
#endif
	double *n_k;
    	double *L1_res_vec, *L2_res_vec, *L2_correction;
    	L2_correction = new double [max_particle_size];
	n_k = new double [max_particle_size];

	//double coefficient = -0.5;

	//DATA FOR PYTHON VISUALIZER BEGIN
	ofstream velos;
	if(rank==0 && if_paint) velos.open("velocities.txt");
	ofstream verts;
	if(rank==0 && if_paint) verts.open("vertices.txt");
	ofstream faces;
	if(rank==0 && if_paint) faces.open("faces.txt");
	ofstream colours;
	if(rank==0 && if_paint) colours.open("colours.txt");
	if(rank==0)
	{
		for (auto it(vertices.begin()); it != vertices.end(); it++)
		{
			verts << (*it)->x() << " " << (*it)->y() << " 0.0" << endl; 
		}
		verts.close();
		for (auto it(visual.begin()); it != visual.end(); it++)
		{
			for (int i = 0; i < 3; i++)
			{
				auto vert = find(vertices.begin(), vertices.end(),(*it)->getCorner(i));
				if (vert != vertices.end())  faces << distance(vertices.begin(), vert) << " ";
				else cout << "SOMETHING HAPPENED" << endl;
			}
			faces << endl;
		}
		faces.close();
	}
	//DATA FOR PYTHON VISUALIZE END
	if(rank==0) 
	{
		//Visualizer2 visualizer("zone_visualizer.ps");
		//for (auto it(visual.begin()); it != visual.end(); it++)
		//{
		//	visualizer.addObject(**it, Color(1,0,0,0.02, true));
		//}
		GRID.show("square.ps");
		//visualizer.writeFile();
	}

	if(rank==0) cout << "THE NUMBER OF triangles IS " << visual.size() << endl;
	ofstream bottom;
	if(rank==0)
	{
		bottom.open("bottom.txt");
		bottom << setprecision(6) << fixed;
	}
	//cout << rank << " "<< steps[0]*coord_x << " " << steps[0]*(coord_x+1.0) << " " << steps[1]*(coord_y+1.0) << " " << steps[1]*(coord_y) << endl;
	unique_ptr<advection_2d_mpi> equation = make_unique<advection_2d_mpi>(rank, x, y, steps[0]*(coord_x), steps[0]*(coord_x+1.0), steps[1]*(coord_y+1.0), steps[1]*(coord_y), visual, visual_core);
	Vector2 * velocities = new Vector2 [visual_core.size()*max_particle_size];
	for (int i = 0; i < max_particle_size; i++)
	{
		int p = 0;
		for (auto it(visual_core.begin()); it != visual_core.end(); it++)
		{
			double val = length_y;
			for (int j = 0; j < 3; j++) val = min(val, (*it)->getCorner(j)->y());
			velocities[p + i * visual_core.size()] = Vector2(init_velocity_x*val/length_y, -nominal_velocity_y*pow(h*(i+1), 2.0/3.0));
			//if (velocities[p + i * visual_core.size()].x()==0.0) cout << "it happened\n"; 
			p++;
		}
	}
	double *u;
	u = new double [visual_core.size()*max_particle_size];

	double *u_collect;
	if (rank == 0)
	{
		u_collect = new double [visual.size()*max_particle_size];
	}

	double *smoluch_operator;
	smoluch_operator = new double [visual_core.size()*max_particle_size];
    	double *initial_layer;
	initial_layer = new double [visual_core.size()*max_particle_size];

	Point2 source;
	if (nominal_velocity_y > 0.0) source = Point2(0.0, length_y);
	else source =  Point2(0.3, 0.0);
	advection_init_smooth(u, visual_core, max_particle_size, h, source);


	for (int i=0;i<visual_core.size();i++)
	{
        	for (int m=0;m<max_particle_size;m++)
		{
            		smoluch_operator[i+m*visual_core.size()]=u[i+m*visual_core.size()];
			initial_layer[i+m*visual_core.size()]=u[i+m*visual_core.size()];
        	}
    	}
	MPI_Barrier(comm);
	double start;
	if(rank == 0) start = MPI_Wtime();

	for (int t = 0; t < TIME; t++)
	{	
		if (rank==0) cout << t << endl;
		if (t % periods == 0 && if_paint)
		{
    			if (rank==0)
			{ 
				double *buffer;
        			for (int i = size_of_group-1; i >= 0; i--)
				{
					if (i != 0 )
					{
						MPI_Status status;
						MPI_Probe(i, 1, comm, &status);
						int size = 0;
						MPI_Get_count(&status, MPI_DOUBLE, &size);
						buffer = new double [size];
						MPI_Recv(buffer, size, MPI_DOUBLE, i, 1, comm, &status);
						for (int j = 0; j < max_particle_size; j++)
						{
							int x_size = size/max_particle_size;
							//cout << "size" << x_size << endl;
							for (int k = 0; k < x_size; k++)
							{	
								u_collect[ids[i][k]+j*visual.size()] = buffer[k+j*x_size];
							}
						}
						delete[] buffer;
					}
					else
					{
						for(int j = 0; j < max_particle_size; j++)
							for(int k = 0; k < visual_core.size(); k++)
								u_collect[ids[i][k]+j*visual.size()] = u[k+j*visual_core.size()];
					}
				}
    			} 
			else 
			{
            			MPI_Ssend(u, max_particle_size*(visual_core.size()), MPI_DOUBLE, 0, 1, comm);
    			}
		}
		if (t%periods == 0 && if_paint && rank==0) 
		{
			for (int k = 0; k < visual.size(); k++)
			{
				for (int i = 0; i < 10; i++)
				{
					int val = max_particle_size/10;
					int index = k+visual.size()*i*val;
					if (u_collect[index] <= 0.25)
					{
						colours << 0.0 << " " << (u_collect[index]/0.25)  << " " << 1.0 << " " << 1.0 << " "; 
					}
					else if (u_collect[index] <= 0.5)
					{
						colours << 0.0 << " " << 1.0 << " " << ((0.5-u_collect[index])/0.25) << " " << 1.0 << " "; 
					}
					else if (u_collect[index] <= 0.75)
					{
						colours << ((u_collect[index]-0.5)/0.25) << " " << 1.0 << " " << 0.0 << " " << 1.0 << " "; 
					}
					else
					{
						colours << 1.0 << " " << ((1.0-u_collect[index])/0.25) << " " << 0.0 << " " << 1.0 << " "; 
					}
					//velos << -(velocities[index].y()/10.0+visual[k]->getBarycenter().y()) << " " << (velocities[index].x()/10.0+visual[k]->getBarycenter().x())<< " " << 0.0 << " ";					
				}
				colours << endl;
				//velos << endl;
			}
			int k = 0;
			for (auto it(visual.begin()); it != visual.end(); it++)
			{
				bool if_bottom = false;
				for (int i = 0; i < max_particle_size; i++)
				{
					int counter = 0;
					for (int j = 0; j < 3; j++) if ((*it)->getCorner(j)->y() == 0.0) counter++;
					if (counter==1) if_bottom = true;
					if (if_bottom) bottom << (i+1)*h << " " << (*it)->getBarycenter().x() << " " << u[k+i*visual.size()] << endl;
				}
				k++;
			}
			bottom << endl << endl;
		}
		for (int j=0;j<visual_core.size();j++)
		{
			if (equation->is_boundary[j] == false && equation->is_no_slip[j] == false)
			{
				for (int m = 0; m < max_particle_size; m++) n_k[m] =u[j+m*visual_core.size()];        			
				L2_res_vec = crossed_kernel.matvec(n_k);
#ifdef FFTW
            			L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k, ub, vb, plan_v, plan_u, plan_inverse);
#endif
#ifdef MKL_FFT
				L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k, tasks);
#endif
				for (int i = 0; i < max_particle_size; i++)
            			{
                			L2_correction[i] = ( K( i, 1, h ) * n_k[0] + K(i, max_particle_size, h) * n_k[max_particle_size - 1] ) * h;
            			}
				cblas_daxpy(max_particle_size, -0.5, L2_correction, 1, L2_res_vec, 1);

				for (int m = 0; m < max_particle_size; m++)
            			{
                			smoluch_operator[j+m*visual_core.size()] = ( L1_res_vec[m] * 0.5 - n_k[m] * L2_res_vec[m] );
       		 			if (smoluch_operator[j+m*visual_core.size()] < 0.0) smoluch_operator[j+m*visual_core.size()] = 0.0;
            			}
				delete [] L2_res_vec;
				delete [] L1_res_vec;
			}
        	}
		equation->solver(u, dt, velocities, max_particle_size);

		for (int i=0;i<visual_core.size();i++)
		{
            		for (int m=0;m<max_particle_size;m++)
			{
               			u[i+m*visual_core.size()] = initial_layer[i+m*visual_core.size()] + dt * (smoluch_operator[i+m*visual_core.size()] - u[i+m*visual_core.size()] - (equation->is_source[i] ? 0.0 : K_evap*initial_layer[i+m*visual_core.size()]) );
				if (u[i+m*visual_core.size()] > 1.0) u[i+m*visual_core.size()] = 1.0;
				else if (u[i+m*visual_core.size()] < 0.0) u[i+m*visual_core.size()] = 0.0;
            		}
        	}

		//********************************TO*REMOVE*PERIODIC*COMMENT*THIS**************************
		advection_periodic(u, visual_core, max_particle_size, h, source, t, equation->is_source);
		//*****************************************************************************************


		for (int i=0;i<visual_core.size();i++)
		{
            		for (int m=0;m<max_particle_size;m++)
			{
				initial_layer[i+m*visual_core.size()] = u[i+m*visual_core.size()];
				smoluch_operator[i+m*visual_core.size()] = u[i+m*visual_core.size()];
            		}
        	}
		
	}
	MPI_Barrier(comm);	
	double end;
	if (rank == 0)
	{
		end = MPI_Wtime();
		cout << "duration " << end-start << endl;
	}
	delete [] u;		
	delete [] smoluch_operator;
	delete [] initial_layer;
	delete [] L2_correction;
	delete [] n_k;
#ifdef FFTW
	for (int i = 0; i < R_value; i++)
	{
		fftw_destroy_plan(plan_v[i]);
		fftw_destroy_plan(plan_u[i]);
		fftw_destroy_plan(plan_inverse[i]);	
	}
	fftw_free(vb);
	fftw_free(ub);
	fftw_free(plan_u);
	fftw_free(plan_v);
	fftw_free(plan_inverse);
#endif
#ifdef MKL_FFT
	for (int r = 0; r < R_value; r++)
	{
		vslConvDeleteTask(tasks + r);
	}
#endif
	if (rank == 0 && if_paint) colours.close();
	if (rank == 0 && if_paint) velos.close();
	if (rank == 0 && if_paint) bottom.close();
	MPI_Finalize();
	return 0;
}

