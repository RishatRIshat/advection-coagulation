#include <algorithm>
#include <memory>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <iomanip>
#include "fade2d/Fade_2D.h"
#include <cassert>
#include <utility>
#include "mesh/example_meshes.h"
#include "coagulation/tensor_train.h"
#include "advection/transport.h"
#include "velocity/velocity.h"
#include "wrappers.h"
using namespace GEOM_FADE2D;
using namespace std;
using namespace example_meshes;
using namespace advection;
using namespace wrappers;


#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()

int main(int argc, char ** argv)
{
	double h, dr, dt;
	int TIME, max_particle_size, TOTAL_FRAMES;
	double length_x, length_y;
	double nominal_velocity_y, init_velocity_x;

	double volume_fraction, gravity, particle_density, fluid_density, fluid_viscosity;

	if(argc !=2) {cout << "need filename" << endl; return 2;}

	string filename{argv[1]};
	ifstream arguments;
	arguments.open(filename);
	vector<string> data;
	string line;
	int i = 0;
	while(getline(arguments, line))
	{
		data.push_back(line);
		i++;
	}
	if (data.size() != 14) {cout << "wrong data" << endl; return 2;}
	h = stod(data[0]);//0.05
	dr = stod(data[1]);//0.04
	dt = stod(data[2]);//0.001
	TIME = stoi(data[3]);//1
	max_particle_size = stoi(data[4]);//50
	TOTAL_FRAMES = stoi(data[5]);//100
	length_x = stod(data[6]);//2.0
	length_y = stod(data[7]);//0.5
	init_velocity_x = stod(data[8]);

	volume_fraction = stod(data[9]);
	gravity = stod(data[10]);
	particle_density = stod(data[11]);
	fluid_density = stod(data[12]);
	fluid_viscosity = stod(data[13]);
	
	nominal_velocity_y = 2.0*gravity/(9.0*fluid_viscosity)*(volume_fraction*particle_density - fluid_density)*pow((1.0/volume_fraction), 2.0/3.0)*0.01*0.01;
	cout << "nominal downward velocity is "<< nominal_velocity_y << endl;
	bool if_paint = (TOTAL_FRAMES == 0) ? 0 : 1;
	int periods = (TOTAL_FRAMES == 0) ? TIME : TIME/TOTAL_FRAMES;
	double max_face_length = dr+0.25*dr;
	double min_face_length = dr-0.25*dr;

	vector<Point2*> vertices;
	vector<Triangle2*> visual;
	Fade_2D GRID;
	mesh_one_circle(GRID, visual, vertices, min_face_length, max_face_length);
	//mesh_structured_rect_var(GRID, visual, vertices, min_face_length, max_face_length, length_x, length_y);
	GRID.show("square.ps");
	dim3 block(32);
	dim3 grid_big((max_particle_size*visual.size()+block.x-1)/block.x);
	dim3 grid_small((max_particle_size+block.x-1)/block.x);
    	cublasHandle_t handle;
	cublasCreate(&handle);
	double tolerance = 1e-4;    			
    	TCross_Parallel_v1 crossed_kernel = default_crossed_kernel(tolerance, max_particle_size, h);		
	int R = crossed_kernel.get_rank();
	int Rnum = crossed_kernel.get_rows_number();
	int n[] = { Rnum }; 
	int inembed[] = { 0 };
	int onembed[] = { 0 }; 
	cufftHandle plan;
	cufftPlanMany(&plan, 1, n, inembed, 1, Rnum, onembed, 1, Rnum, CUFFT_Z2Z, R);

	double *n_k;
	checkCudaErrors(cudaMalloc((void **) &n_k, max_particle_size*sizeof(double)));
	double *L1_res_vec, *L2_res_vec, *L2_correction;
	checkCudaErrors(cudaMalloc((void **) &L2_correction, max_particle_size*sizeof(double)));
	cudaMalloc((void**) &L1_res_vec, max_particle_size*sizeof(double));
	cudaMalloc((void**) &L2_res_vec, max_particle_size*sizeof(double));

	//DATA FOR PYTHON VISUALIZER BEGIN
	ofstream velos;
	velos.open("velocities.txt");
	ofstream verts;
	verts.open("vertices.txt");
	ofstream faces;
	faces.open("faces.txt");
	ofstream colours;
	colours.open("colours.txt");
	double coefficient = -0.5;
	for (auto it(vertices.begin()); it != vertices.end(); it++)
	{
		verts << (*it)->x() << " " << (*it)->y() << " 0.0" << endl; 
	}
	verts.close();
	for (auto it(visual.begin()); it != visual.end(); it++)
	{
		for (int i = 0; i < 3; i++)
		{
			auto vert = find(vertices.begin(), vertices.end(),(*it)->getCorner(i));
			if (vert != vertices.end())  faces << distance(vertices.begin(), vert) << " ";
			else cout << "SOMETHING HAPPENED" << endl;
		}
		faces << endl;
	}
	faces.close();
	//DATA FOR PYTHON VISUALIZE END

	cout << "THE NUMBER OF triangles IS " << visual.size() << endl;
	unique_ptr<advection_2d> equation = make_unique<advection_2d>(visual);
	unique_ptr<vorticity_2d> vorticity = make_unique<vorticity_2d>(visual);
	double *u;
	cudaMallocManaged((void **) &u, visual.size()*max_particle_size*sizeof(double));
	double *smoluch_operator;
    	double *initial_layer;
	cudaMalloc((void **) &smoluch_operator, visual.size()*max_particle_size*sizeof(double));
	cudaMalloc((void **) &initial_layer, visual.size()*max_particle_size*sizeof(double));
	double * vorticities = new double [visual.size()];
	double * initial_vorticities = new double [visual.size()];
	if (nominal_velocity_y > 0.0)
	{
		advection_init_smooth(u, visual, max_particle_size, h, Point2(0.0, 0.25));
	}
	else
	{
		advection_init_smooth(u, visual, max_particle_size, h, Point2(0.3, 0.0));
	}
	equalize_data<<<grid_big, block>>>(initial_layer, smoluch_operator, u, max_particle_size, visual.size());
	cudaDeviceSynchronize();
	Vector2 * velocities = new Vector2 [visual.size()];
	Vector2 * velocities_original = new Vector2 [visual.size()*max_particle_size];
	Vector2 * velos_full = new Vector2[visual.size()*max_particle_size];
/*      *********************************************INTERPOLATED************VELOCITIES************BEGIN**************************
	ifstream interp_vels;
	interp_vels.open("vels.txt");
	std::vector< std::vector<double> > velo;
        //string line2;
        while ( getline( interp_vels, line ) ) {
		std::istringstream is( line );
		velo.push_back( 
			std::vector<double>( std::istream_iterator<double>(is),
			std::istream_iterator<double>() ) );
	}

	Vector2 * velocities = new Vector2 [visual.size()*max_particle_size];
	//Vector2 * velocities_original = new Vector2 [visual.size()*max_particle_size];
	for (int i = 0; i < max_particle_size; i++)
	{
		int p = 0;
		for (auto it(visual.begin()); it != visual.end(); it++)
		{
			//double val = length_y;
			//for (int j = 0; j < 3; j++) val = min(val, (*it)->getCorner(j)->y());
			//velocities[p + i * visual.size()] = Vector2(u[p+i*visual.size()], 0.0);
			velocities[p + i * visual.size()] = Vector2(velo[p][0], velo[p][1]);
			//velocities_original[p + i * visual.size()] = Vector2(u[p+i*visual.size()], 0.0);
			p++;
		}
	}
*****************************************************INTERPOLATED**********************VELOCITIES*******************END*********************/

	for (int i = 0; i < max_particle_size; i++)
	{
		int p = 0;
		for (auto it(visual.begin()); it != visual.end(); it++)
		{
			velocities_original[p + i * visual.size()] = Vector2(0.5, 0.0);
			if(i==0) velocities[p/* + i * visual.size()*/] = Vector2(0.5, 0.0);
			p++;
		}
	}
	vorticity->update_velocities(velocities_original, max_particle_size);
	
	clock_t start = clock();
	double duration;
	for (int t = 0; t < TIME; t++)
	{	
		cout << t << endl;
		if (t%(periods) == 0)
		{
			vorticity->calc_vorticities(velocities_original, vorticities);
			vorticity->calc_vorticities(velocities_original, initial_vorticities);
			vorticity->calculate_velocities(velocities, vorticities, dt);
			vorticity->update_velocities(velocities);
			for (int i=0; i < visual.size(); i++) for (int j=0; j< max_particle_size; j++) velos_full[j*visual.size()+i] = velocities[i];
			
		}
		if (t%periods == 0 && if_paint)
		{
			for (int k = 0; k < visual.size(); k++)
			{
				for (int i = 0; i < 10; i++)
				{
					int val = int(max_particle_size/10);
					int index = k+visual.size()*i*val;
					if (u[index] <= 0.25)
					{
						colours << 0.0 << " " << (u[index]/0.25)  << " " << 1.0 << " " << 1.0 << " "; 
					}
					else if (u[index] <= 0.5)
					{
						colours << 0.0 << " " << 1.0 << " " << ((0.5-u[index])/0.25) << " " << 1.0 << " "; 
					}
					else if (u[index] <= 0.75)
					{
						colours << ((u[index]-0.5)/0.25) << " " << 1.0 << " " << 0.0 << " " << 1.0 << " "; 
					}
					else
					{
						colours << 1.0 << " " << ((1.0-u[index])/0.25) << " " << 0.0 << " " << 1.0 << " "; 
					}
					velos << -(velos_full[index].y()/30.0+visual[k]->getBarycenter().y()) << " " << (velos_full[index].x()/30.0+visual[k]->getBarycenter().x())<< " " << 0.0 << " ";					
				}
				colours << endl;
				velos << endl;
			}
		}
		for (int j=0;j<visual.size();j++)
		{
			if (equation->is_boundary[j] == false)
			{
				cudaMemcpy(n_k, &initial_layer[j*max_particle_size], max_particle_size*sizeof(double), cudaMemcpyDeviceToDevice);          			
				crossed_kernel.matvec(n_k, handle, L2_res_vec);
            			crossed_kernel.smol_conv_trapezoids(n_k, plan, handle, L1_res_vec);
				calc_L2_correction<<<grid_small, block>>>(n_k, h, L2_correction, max_particle_size);
				cublasDaxpy(handle, max_particle_size, &coefficient, L2_correction, 1, L2_res_vec, 1);
				calc_smoluch<<<grid_small, block>>>(L1_res_vec, L2_res_vec, n_k, &smoluch_operator[j*max_particle_size], max_particle_size);
			}
        	}
		equation->solver(u, dt, velocities_original, max_particle_size);
		equation->solver(vorticities, dt, velocities_original);
		new_time_step<<<grid_big, block>>>(initial_layer, smoluch_operator, u, max_particle_size, visual.size(), dt);
		equalize_data<<<grid_big, block>>>(initial_layer, smoluch_operator, u, max_particle_size, visual.size());
		cudaDeviceSynchronize();
		for (int i=0;i<visual.size();i++)
		{
            		//for (int m=0;m<max_particle_size;m++)
			//{
				if (!equation->is_boundary[i]){
				vorticities[i/*+m*visual.size()*/] = initial_vorticities[i/*+m*visual.size()*/] - dt * vorticities[i/*+m*visual.size()*/];
				initial_vorticities[i/*+m*visual.size()*/] = vorticities[i/*+m*visual.size()*/];}
            		//}
        	}
		for (int j=0;j<visual.size();j++)
		{
			if (equation->is_boundary[j] == false)
			{
				cudaMemcpy(n_k, &initial_layer[j*max_particle_size], max_particle_size*sizeof(double), cudaMemcpyDeviceToDevice);          			
				crossed_kernel.matvec(n_k, handle, L2_res_vec);
            			crossed_kernel.smol_conv_trapezoids(n_k, plan, handle, L1_res_vec);
				calc_L2_correction<<<grid_small, block>>>(n_k, h, L2_correction, max_particle_size);
				cublasDaxpy(handle, max_particle_size, &coefficient, L2_correction, 1, L2_res_vec, 1);
				calc_smoluch<<<grid_small, block>>>(L1_res_vec, L2_res_vec, n_k, &smoluch_operator[j*max_particle_size], max_particle_size);
			}
        	}
		equation->solver(u, dt, velos_full, max_particle_size);
		equation->solver(vorticities, dt, velocities);
		new_time_step<<<grid_big, block>>>(initial_layer, smoluch_operator, u, max_particle_size, visual.size(), dt);
		equalize_data<<<grid_big, block>>>(initial_layer, smoluch_operator, u, max_particle_size, visual.size());
		cudaDeviceSynchronize();
		for (int i=0;i<visual.size();i++)
		{
            		//for (int m=0;m<max_particle_size;m++)
			//{
				if (!equation->is_boundary[i]){
				vorticities[i] = initial_vorticities[i] - dt * vorticities[i];
				initial_vorticities[i] = vorticities[i];}
            		//}
        	}
	}
	
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	cout << "duration " << duration << endl;
	checkCudaErrors(cudaFree(u));
	checkCudaErrors(cudaFree(smoluch_operator));
	checkCudaErrors(cudaFree(initial_layer));
	cublasDestroy(handle);
	colours.close();
	velos.close();
	return 0;
}

