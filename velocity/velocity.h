#pragma once

#include <math.h>	

void solve_linear_system_eigen(double *& matrix, double *&result, double *&right_hand_side, int size);

#include <stdio.h>
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include "../fade2d/Fade_2D.h"

using namespace GEOM_FADE2D;
using namespace std;

namespace advection
{
#ifndef CUDA_ADV
	double sigma(double const &r, double const &dr, int const &PML, int const & size);
#endif
#ifdef CUDA_ADV
		__host__ __device__ double sigma(double const &r, double const &dr, int const &PML, int const & size);
#endif
	class vorticity_2d
	{
	protected:
		int size;
		double * xbarys;
		Vector2 * face_normals;
		Vector2 * barycenter_dist_vector;
		double * tr_areas;
		int * neighbor_ids;
		double * inverse_weights;
#ifdef CUDA_ADV
		__host__ __device__ double limiter(double const& r_factor, double const& weight);
#endif
#ifndef CUDA_ADV
		double limiter(double const& r_factor, double const& weight);
#endif
	public:
		bool * is_boundary;
	
		void find_gradients(Vector2 * values, double * input);
		void calc_dynamic_arrays(vector<Triangle2*> triangles);
		vorticity_2d(vector<Triangle2*> triangles) : size(triangles.size())
		{
			calc_dynamic_arrays(triangles);
		}
		~vorticity_2d();
		vorticity_2d(const vorticity_2d&) = delete;
		vorticity_2d& operator= (const vorticity_2d&) = delete;
		vorticity_2d& operator= (const vorticity_2d&&) = delete;
		vorticity_2d(const vorticity_2d&&) = delete;
		void update_velocities(Vector2 *& velocities, const int& batch = 1);
		void calc_vorticities(Vector2 *& velocities, double *& vorticities, const int& batch = 1);
		void calculate_velocities(Vector2 *& velocities, double *& vorticities, double const & dt, const int& batch = 1);
	};
}
