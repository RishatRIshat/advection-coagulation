

#ifdef CUDA_ADV
cusolverSpHandle_t solver_handle;
#endif

advection_2d(vector<Triangle2*> triangles) : size(triangles.size()) 
{
	#ifdef CUDA_ADV
	cusolverSpCreate(&solver_handle);
	#endif
	calc_dynamic_arrays(triangles);
}

//void calculate_velocities(Vector2 *& velocities_original, Vector2 *& velocities, double *& vorticities, double const & dt, const int& batch = 1);

void update_velocities(Vector2 *& velocities, const int& batch = 1);

void solve_linear_system_owl(cusolverSpHandle_t & solver_handle, int size, double const * h_A_dense, double const * h_y, double * h_x);
