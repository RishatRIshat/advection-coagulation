#include "wrappers.h"
#include <cmath>

namespace wrappers
{
#ifndef CUDA_FFT
	/*double K(const int & u, const int &v, const double h)
	{
   		double u1=(u+1.0) * h;
    		double v1=(v+1.0) * h;
		
		double coef = 0.0001;
		double result = 0;
		//******************NON-FLUX DIFFUSION KERNEL START**************
		//result = 4*100*(coef/u1+coef/v1)*(u1+v1);
		//******************NON-FLUX DIFFUSION KERNEL END****************
		//***************************************************************
		//**********************FLUX DIFFUSION KERNEL START**************
		//double v0 = 1.0;
		//double arg = v0/(2*(coef/u1+coef/v1))*(u1+v1);
		//for (int i = 0; i < 10; i++) result += (((double)i)/arg*std::cyl_bessel_k(i,arg)-std::cyl_bessel_k(i+1, arg))/(std::cyl_bessel_k(i, arg));
		//result *= -pow(u1+v1,2)*v0;
		//**********************FLUX DIFFUSION KERNELE END***************
    		//result = pow(pow(u1, 1.0 / 3) + pow(v1, 1.0 / 3), 2) * sqrt((double)(1.0 / u1 + 1.0 / v1));
    		result = 2.25*10.0*pow(u1+v1, 0.5)/pow(u1*v1, 1.0/3.0);
		return result;
	}*/
	//***************************THIS IS NONFLUX************************************************
	//******************************************************************************************
	double K_nonflux(const int & u, const int &v, const double h)
	{
   		double u1=(u+1.0) * h;
    		double v1=(v+1.0) * h;
		
		double coef = 0.0001;
		double result = 0;
		//******************NON-FLUX DIFFUSION KERNEL START**************
		result = 100*4*10*2.25*(coef/u1+coef/v1)*(u1+v1);
		//******************NON-FLUX DIFFUSION KERNEL END****************
		//***************************************************************
		//**********************FLUX DIFFUSION KERNEL START**************
		//double v0 = 1.0;
		//double arg = v0/(2*(coef/u1+coef/v1))*(u1+v1);
		//for (int i = 0; i < 10; i++) result += (((double)i)/arg*std::cyl_bessel_k(i,arg)-std::cyl_bessel_k(i+1, arg))/(std::cyl_bessel_k(i, arg));
		//result *= -pow(u1+v1,2)*v0;
		//**********************FLUX DIFFUSION KERNELE END***************
		return result;
	}
	//******************************************************************************************
	
	//*************************THIS IS FLUX*****************************************************
	//******************************************************************************************
	double K(const int & u, const int &v, const double h)
	{
   		double u1=(u+1.0) * h;
    		double v1=(v+1.0) * h;
		
		double coef = 0.0001;
		double result = 0;
		//******************NON-FLUX DIFFUSION KERNEL START**************
		//result = 4*100*10*2.25*(coef/u1+coef/v1)*(u1+v1);
		//******************NON-FLUX DIFFUSION KERNEL END****************
		//***************************************************************
		//**********************FLUX DIFFUSION KERNEL START**************
		//double v0 = 1.000001;
		double v0 = 0.5;
		double arg = v0/(2*(coef/u1+coef/v1))*(u1+v1);
		for (int i = 0; i < 10; i++) result += (((double)i)/arg*std::cyl_bessel_k(i,arg)-std::cyl_bessel_k(i+1, arg))/(std::cyl_bessel_k(i, arg));
		result *= -pow(u1+v1,2)*v0*100;
		//**********************FLUX DIFFUSION KERNELE END***************
		return result;
	}
	//******************************************************************************************
	//******************************************************************************************
#endif
	TCross_Parallel_v1 default_crossed_kernel(const double & tolerance, const int & size, const double & dm)
	{
		TCross_Parallel_v1_Parameters parameters;
    		parameters.tolerance = tolerance;
    		parameters.maximal_iterations_number = 0;
	    	TKernel kernel(size, size);
    		kernel.h = dm;
    		TCross_Parallel_v1 crossed_kernel;
    		crossed_kernel.Approximate(&kernel, parameters);
		return crossed_kernel;
	}

	void advection_periodic(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord, int t, bool * is_source)
	{	
		for (int i = 0; i < max_particle_size; i++)
		{
			int k = 0;
			for (auto it(triangles.begin()); it != triangles.end(); it++)
			{
				if (i <= max_particle_size/2 && is_source[k]) 
				{
					//std::cout << "works for: " << (*it)->getBarycenter().x() << " " << (*it)->getBarycenter().y() << std::endl;
					u[k+i*triangles.size()] = abs(sin(t*0.0025*3.1416))*exp(-pow((*it)->getBarycenter().y()-source_coord.y(),2)*100.0-pow((*it)->getBarycenter().x()-source_coord.x(),2)*100.0-pow((i+1)*h,2));
				}
				else
				{
					//u[k+i*triangles.size()] = u[k+i*triangles.size()];
					//if (i<=max_particle_size/2) std::cout << "does not work for: " << (*it)->getBarycenter().x() << " " << (*it)->getBarycenter().y() << std::endl;
				}
				k++;
			}
		}
	}

	void advection_init_smooth(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord)
	{	
		for (int i = 0; i < max_particle_size; i++)
		{
			int k = 0;
			for (auto it(triangles.begin()); it != triangles.end(); it++)
			{	
				if (i <= max_particle_size/2) u[k+i*triangles.size()] = exp(-pow((*it)->getBarycenter().y()-source_coord.y(),2)*100.0-pow((*it)->getBarycenter().x()-source_coord.x(),2)*100.0-pow((i+1)*h,2));
				else u[k+i*triangles.size()] = 0.0;
				k++;
			}
		}
	}

	void advection_init_smooth_disalligned(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord)
	{	
		for (int i = 0; i < max_particle_size; i++)
		{
			int k = 0;
			for (auto it(triangles.begin()); it != triangles.end(); it++)
			{	
				u[k*max_particle_size+i] = exp(-pow((*it)->getBarycenter().y()-source_coord.y(),2)*100.0-pow((*it)->getBarycenter().x()-source_coord.x(),2)*500.0-pow((i+1)*h,2));
				k++;
			}
		}
	}

	void velocity_init_potential(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord)
	{
//		for (int i = 0; i < max_particle_size; i++)
//		{
//			int k = 0;
//			for (auto it(triangles.begin()); it != triangles.end(); it++)
//			{	
//				u[k+i*triangles.size()] = exp(-pow((*it)->getBarycenter().y()-source_coord.y(),2)*500.0-pow((*it)->getBarycenter().x()-source_coord.x(),2)*100.0-pow((i+1)*h,2));
//				k++;
//			}
//		}
	}

	void advection_init_setuha(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord)
	{
		for (int i = 0; i < max_particle_size; i++)
		{
			int k = 0;
			for (auto it(triangles.begin()); it != triangles.end(); it++)
			{	
				if ((*it)->getBarycenter().x() < 0.05 && (*it)->getBarycenter().y() > 0.125 && (*it)->getBarycenter().y() < 0.375) u[k+i*triangles.size()] = exp(-pow((*it)->getBarycenter().y()-source_coord.y(),2)*100.0-pow((*it)->getBarycenter().x()-source_coord.x(),2)*500.0-pow((i)*h,2));
				else u[k+i*triangles.size()] = 0.0;
				k++;
			}
		}
	}

	void advection_init_setuha_full(double * u, vector<Triangle2*> const& triangles, int max_particle_size, double h, Point2 const& source_coord)
	{
		cout << "called full" << endl;
		for (int i = 0; i < max_particle_size; i++)
		{
			int k = 0;
			for (auto it(triangles.begin()); it != triangles.end(); it++)
			{	
				if ((*it)->getBarycenter().x() < 0.05 && (*it)->getBarycenter().y() > 0.0 && (*it)->getBarycenter().y() < 0.5) u[k+i*triangles.size()] = exp(-pow((*it)->getBarycenter().y()-source_coord.y(),2)*25.0-pow((*it)->getBarycenter().x()-source_coord.x(),2)*500.0-pow((i)*h,2));
				else u[k+i*triangles.size()] = 0.0;
				k++;
			}
		}
	}

	
}

