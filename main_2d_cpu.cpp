#include <algorithm>
#include <memory>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <iomanip>
#include "fade2d/Fade_2D.h"
#include <cassert>
#include <utility>
#include "mesh/example_meshes.h"
#include "advection/transport.h"
#include "velocity/velocity.h"
#include "wrappers.h"
using namespace GEOM_FADE2D;
using namespace std;
using namespace example_meshes;
using namespace advection;
using namespace wrappers;
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()

int main(int argc, char ** argv)
{
	double h, dr, dt;
	int TIME, max_particle_size, TOTAL_FRAMES;
	double length_x, length_y;
	double nominal_velocity_y, init_velocity_x;

	double volume_fraction, gravity, particle_density, fluid_density, fluid_viscosity;

	if(argc !=2) {cout << "need filename" << endl; return 2;}

	string filename{argv[1]};
	ifstream arguments;
	arguments.open(filename);
	vector<string> data;
	string line;
	int i = 0;
	while(getline(arguments, line))
	{
		data.push_back(line);
		i++;
	}
	if (data.size() != 14) {cout << "wrong data" << endl; return 2;}
	h = stod(data[0]);//0.05
	dr = stod(data[1]);//0.04
	dt = stod(data[2]);//0.001
	TIME = stoi(data[3]);//1
	max_particle_size = stoi(data[4]);//50
	TOTAL_FRAMES = stoi(data[5]);//100
	length_x = stod(data[6]);//2.0
	length_y = stod(data[7]);//0.5
	init_velocity_x = stod(data[8]);

	volume_fraction = stod(data[9]);
	gravity = stod(data[10]);
	particle_density = stod(data[11]);
	fluid_density = stod(data[12]);
	fluid_viscosity = stod(data[13]);
	
	nominal_velocity_y = 2.0*gravity/(9.0*fluid_viscosity)*(volume_fraction*particle_density - fluid_density)*pow((1.0/volume_fraction), 2.0/3.0)*0.01*0.01;
	cout << "nominal downward velocity is "<< nominal_velocity_y << endl;
	bool if_paint = (TOTAL_FRAMES == 0) ? 0 : 1;
	int periods = (TOTAL_FRAMES == 0) ? TIME : TIME/TOTAL_FRAMES;
	double max_face_length = dr+0.25*dr;
	double min_face_length = dr-0.25*dr;

	vector<Point2*> vertices;
	vector<Triangle2*> visual;
	Fade_2D GRID;
	//mesh_three_circles(GRID, visual, vertices, 0.01, 0.05);
	mesh_one_circle(GRID, visual, vertices, min_face_length, max_face_length);
	//setuha(GRID, visual, vertices, min_face_length, max_face_length, 30);
	//mesh_unstructured_square(GRID, visual, vertices, 0.01, 0.05);
	//mesh_unstructured_rect(GRID, visual, vertices, min_face_length, max_face_length);
	//mesh_structured_rect_var(GRID, visual, vertices, min_face_length, max_face_length, length_x, length_y);
	GRID.show("square.ps");

	double tolerance = 1e-4;    			
    	TCross_Parallel_v1 crossed_kernel = default_crossed_kernel(tolerance, max_particle_size, h);		
	//FFTW START
	double R_value = crossed_kernel.get_rank();
	double V_value = crossed_kernel.get_columns_number();
	fftw_complex *ub = (fftw_complex *) fftw_malloc(R_value * V_value * sizeof(fftw_complex));
      	fftw_complex *vb = (fftw_complex *) fftw_malloc(R_value * V_value * sizeof(fftw_complex));
	fftw_plan * plan_v = (fftw_plan *) fftw_malloc(R_value * sizeof(fftw_plan));
	fftw_plan * plan_u = (fftw_plan *) fftw_malloc(R_value * sizeof(fftw_plan));
	fftw_plan * plan_inverse = (fftw_plan *) fftw_malloc(R_value * sizeof(fftw_plan));
	for (int i = 0; i < R_value; i++)
	{
		plan_v[i] = fftw_plan_dft_1d(max_particle_size, vb+i*max_particle_size, vb+i*max_particle_size, FFTW_FORWARD, FFTW_ESTIMATE);
		plan_u[i] = fftw_plan_dft_1d(max_particle_size, ub+i*max_particle_size, ub+i*max_particle_size, FFTW_FORWARD, FFTW_ESTIMATE);
		plan_inverse[i] = fftw_plan_dft_1d(max_particle_size, ub+i*max_particle_size, ub+i*max_particle_size, FFTW_BACKWARD, FFTW_ESTIMATE);	
	}
	//FFTW END

	double *n_k;
    	double *L1_res_vec, *L2_res_vec, *L2_correction;
    	L2_correction = new double [max_particle_size];
	n_k = new double [max_particle_size];


	//DATA FOR PYTHON VISUALIZER BEGIN
	ofstream velos;
	velos.open("velocities.txt");
	ofstream verts;
	verts.open("vertices.txt");
	ofstream faces;
	faces.open("faces.txt");
	ofstream colours;
	colours.open("colours.txt");
	ofstream vert_colours;
	vert_colours.open("vort_colors.txt");
	for (auto it(vertices.begin()); it != vertices.end(); it++)
	{
		verts << (*it)->x() << " " << (*it)->y() << " 0.0" << endl; 
	}
	verts.close();
	for (auto it(visual.begin()); it != visual.end(); it++)
	{
		for (int i = 0; i < 3; i++)
		{
			auto vert = find(vertices.begin(), vertices.end(),(*it)->getCorner(i));
			if (vert != vertices.end())  faces << distance(vertices.begin(), vert) << " ";
			else cout << "SOMETHING HAPPENED" << endl;
		}
		faces << endl;
	}
	faces.close();
	
	ofstream bottom;
	bottom.open("bottom.txt");
	bottom << setprecision(6) << fixed;

	//DATA FOR PYTHON VISUALIZE END

	cout << "THE NUMBER OF triangles IS " << visual.size() << endl;
	unique_ptr<advection_2d> equation = make_unique<advection_2d>(visual);
	unique_ptr<vorticity_2d> vorticity = make_unique<vorticity_2d>(visual);
	double *u = new double [visual.size()*max_particle_size];
    	double *initial_layer = new double [visual.size()*max_particle_size];
	double *smoluch_operator;
	smoluch_operator = new double [visual.size()*max_particle_size];
	double * vorticities = new double [visual.size()*max_particle_size];
	double * initial_vorticities = new double [visual.size()*max_particle_size];
	if (nominal_velocity_y > 0.0)
	{
		advection_init_smooth(u, visual, max_particle_size, h, Point2(0.0, 0.25));
		advection_init_smooth(initial_layer, visual, max_particle_size, h, Point2(0.0, 0.25));
		advection_init_smooth(smoluch_operator, visual, max_particle_size, h, Point2(0.0, 0.25));
	}
	else
	{
		advection_init_smooth(u, visual, max_particle_size, h, Point2(0.3, 0.0));
		advection_init_smooth(initial_layer, visual, max_particle_size, h, Point2(0.3, 0.0));
		advection_init_smooth(smoluch_operator, visual, max_particle_size, h, Point2(0.3, 0.0));
	}

	Vector2 * velocities = new Vector2 [visual.size()*max_particle_size];
	Vector2 * velocities_original = new Vector2 [visual.size()*max_particle_size];
	for (int i = 0; i < max_particle_size; i++)
	{
		int p = 0;
		for (auto it(visual.begin()); it != visual.end(); it++)
		{
			velocities_original[p + i * visual.size()] = Vector2(0.5, 0.0);
			velocities[p + i * visual.size()] = Vector2(0.5, 0.0);
			p++;
		}
	}
	/*for (int i = 0; i < max_particle_size; i++)
	{
		int p = 0;
		for (auto it(visual.begin()); it != visual.end(); it++)
		{
			double val = length_y;
			for (int j = 0; j < 3; j++) val = min(val, (*it)->getCorner(j)->y());
			velocities[p + i * visual.size()] = Vector2(init_velocity_x*val/length_y, -nominal_velocity_y*pow(h*(i+1), 2.0/3.0));
			p++;
		}
	}*/
	vorticity->update_velocities(velocities_original, max_particle_size);

	clock_t start = clock();
	double duration;
	for (int t = 0; t < TIME; t++)
	{
		cout << t << endl;
		if (t%(periods) == 0)
		{
			vorticity->calc_vorticities(velocities_original, vorticities, max_particle_size);
			vorticity->calc_vorticities(velocities_original, initial_vorticities, max_particle_size);
			vorticity->calculate_velocities(velocities, vorticities, dt, max_particle_size);		
			vorticity->update_velocities(velocities, max_particle_size);
		}
		if (t%periods == 0 && if_paint) 
		{
			double maximum = 0.0;
			double minimum = 0.0;
			double average = 0.0;
			for (int k = 0; k < visual.size(); k++)
			{
				if (vorticities[k] > maximum) maximum = vorticities[k];
				if (vorticities[k] < minimum) minimum = vorticities[k];
				average+=vorticities[k];
			}
			average = average/visual.size();
			cout << minimum << endl;
			cout << maximum << endl;
			cout << average << endl;
			double * vorticities_n = new double [visual.size()];
			

			for (int k = 0; k < visual.size(); k++)
			{
				vorticities_n[k] = (vorticities[k] - minimum)/(maximum-minimum);
			}
		
			for (int k = 0; k < visual.size(); k++)
			{
				int index = k;
				if (vorticities_n[index] <= 0.25)
				{
					vert_colours << 0.0 << " " << (vorticities_n[index]/0.25)  << " " << 1.0 << " " << 1.0 << " "; 
				}
				else if (vorticities_n[index] <= 0.5)
				{
				vert_colours << 0.0 << " " << 1.0 << " " << ((0.5-vorticities_n[index])/0.25) << " " << 1.0 << " "; 
				}
				else if (vorticities_n[index] <= 0.75)
				{
					vert_colours << ((vorticities_n[index]-0.5)/0.25) << " " << 1.0 << " " << 0.0 << " " << 1.0 << " "; 
				}
				else
				{
					vert_colours << 1.0 << " " << ((1.0-vorticities_n[index])/0.25) << " " << 0.0 << " " << 1.0 << " "; 
				}
				vert_colours << endl;
			}


			for (int k = 0; k < visual.size(); k++)
			{
				for (int i = 0; i < 10; i++)
				{
					int index = k+visual.size()*i*(max_particle_size/10);
					if (u[index] <= 0.25)
					{
						colours << 0.0 << " " << (u[index]/0.25)  << " " << 1.0 << " " << 1.0 << " "; 
					}
					else if (u[index] <= 0.5)
					{
						colours << 0.0 << " " << 1.0 << " " << ((0.5-u[index])/0.25) << " " << 1.0 << " "; 
					}
					else if (u[index] <= 0.75)
					{
						colours << ((u[index]-0.5)/0.25) << " " << 1.0 << " " << 0.0 << " " << 1.0 << " "; 
					}
					else
					{	
						colours << 1.0 << " " << ((1.0-u[index])/0.25) << " " << 0.0 << " " << 1.0 << " "; 
					}
					velos << -(velocities[index].y()/30.0+visual[k]->getBarycenter().y()) << " " << (velocities[index].x()/30.0+visual[k]->getBarycenter().x())<< " " << 0.0 << " ";					
				}
				colours << endl;
				velos << endl;
			}
			int k = 0;
			for (auto it(visual.begin()); it != visual.end(); it++)
			{
				bool if_bottom = false;
				for (int i = 0; i < max_particle_size; i++)
				{
					int counter = 0;
					for (int j = 0; j < 3; j++) if ((*it)->getCorner(j)->y() == 0.0) counter++;
					if (counter==2) if_bottom = true;
					if (if_bottom) bottom << (i+1)*h << " " << (*it)->getBarycenter().x() << " " << u[k+i*visual.size()] << endl;
				}
				k++;
			}
			bottom << endl << endl;
		}

		for (int j = 0; j < visual.size(); j++)
		{
			if (equation->is_boundary[j] == false && equation->is_no_slip[j] == false)
			{
				for (int m = 0; m < max_particle_size; m++) n_k[m] =u[j+m*visual.size()];        			
				L2_res_vec = crossed_kernel.matvec(n_k);
            			L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k, ub, vb, plan_v, plan_u, plan_inverse);
				for (int i = 0; i < max_particle_size; i++)
            			{
                			L2_correction[i] = ( K( i, 1, h ) * n_k[0] + K(i, max_particle_size, h) * n_k[max_particle_size - 1] ) * h;
            			}
				cblas_daxpy(max_particle_size, -0.5, L2_correction, 1, L2_res_vec, 1);

				for (int m = 0; m < max_particle_size; m++)
            			{
                			smoluch_operator[j+m*visual.size()] = ( L1_res_vec[m] * 0.5 - n_k[m] * L2_res_vec[m] );
       		 			if (smoluch_operator[j+m*visual.size()] < 0.0) smoluch_operator[j+m*visual.size()] = 0.0;
            			}
				delete [] L2_res_vec;
				delete [] L1_res_vec;
			}
		}
		equation->solver(u, dt, velocities_original, max_particle_size);
		equation->solver(vorticities, dt, velocities_original, max_particle_size);
		for (int i=0;i<visual.size();i++)
		{
            		for (int m=0;m<max_particle_size;m++)
			{
				if (!equation->is_boundary[i]){
               			u[i+m*visual.size()] = initial_layer[i+m*visual.size()] + dt * (smoluch_operator[i+m*visual.size()] - u[i+m*visual.size()]);
				vorticities[i+m*visual.size()] = initial_vorticities[i+m*visual.size()] - dt * vorticities[i+m*visual.size()];
				initial_layer[i+m*visual.size()] = u[i+m*visual.size()];
				smoluch_operator[i+m*visual.size()] = u[i+m*visual.size()];
				initial_vorticities[i+m*visual.size()] = vorticities[i+m*visual.size()];}
            		}
        	}
		for (int j = 0; j < visual.size(); j++)
		{
			if (equation->is_boundary[j] == false && equation->is_no_slip[j] == false)
			{
				for (int m = 0; m < max_particle_size; m++) n_k[m] =u[j+m*visual.size()];        			
				L2_res_vec = crossed_kernel.matvec(n_k);
            			L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k, ub, vb, plan_v, plan_u, plan_inverse);
				for (int i = 0; i < max_particle_size; i++)
            			{
                			L2_correction[i] = ( K( i, 1, h ) * n_k[0] + K(i, max_particle_size, h) * n_k[max_particle_size - 1] ) * h;
            			}
				cblas_daxpy(max_particle_size, -0.5, L2_correction, 1, L2_res_vec, 1);

				for (int m = 0; m < max_particle_size; m++)
            			{
                			smoluch_operator[j+m*visual.size()] = ( L1_res_vec[m] * 0.5 - n_k[m] * L2_res_vec[m] );
       		 			if (smoluch_operator[j+m*visual.size()] < 0.0) smoluch_operator[j+m*visual.size()] = 0.0;
            			}
				delete [] L2_res_vec;
				delete [] L1_res_vec;
			}
		}
		equation->solver(u, dt, velocities, max_particle_size);
		equation->solver(vorticities, dt, velocities, max_particle_size);
		for (int i=0;i<visual.size();i++)
		{
            		for (int m=0;m<max_particle_size;m++)
			{
				if (!equation->is_boundary[i]){
               			u[i+m*visual.size()] = initial_layer[i+m*visual.size()] + dt * (smoluch_operator[i+m*visual.size()] - u[i+m*visual.size()]);
				vorticities[i+m*visual.size()] = initial_vorticities[i+m*visual.size()] - dt * vorticities[i+m*visual.size()];
				initial_layer[i+m*visual.size()] = u[i+m*visual.size()];
				smoluch_operator[i+m*visual.size()] = u[i+m*visual.size()];
				initial_vorticities[i+m*visual.size()] = vorticities[i+m*visual.size()];}
            		}
        	}
	}
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	cout << "duration " << duration << endl;
	colours.close();
	velos.close();
	delete [] u;		
	delete [] smoluch_operator;
	delete [] initial_layer;
	delete [] L2_correction;
	delete [] n_k;
	for (int i = 0; i < R_value; i++)
	{
		fftw_destroy_plan(plan_v[i]);
		fftw_destroy_plan(plan_u[i]);
		fftw_destroy_plan(plan_inverse[i]);	
	}
	fftw_free(vb);
	fftw_free(ub);
	fftw_free(plan_u);
	fftw_free(plan_v);
	fftw_free(plan_inverse);
	bottom.close();
	return 0;
}

