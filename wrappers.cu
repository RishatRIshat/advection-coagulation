#include "wrappers.h"

namespace wrappers
{
	__device__ double K(const int & u, const int &v, const double h)
	{
	    //return 500.0;
	    double u1=(u+1.0) * h;
	    double v1=(v+1.0) * h;
	    //return pow(pow(u1, 1.0 / 3) + pow(v1, 1.0 / 3), 2) * sqrt((double)(1.0 / u1 + 1.0 / v1));
	    return 5.0*pow(u1+v1, 0.5)/pow(u1*v1, 1.0/3.0);
	}
	__global__ void new_time_step(const double *init, const double *smol, double *result, const int N, const int S, const double dt)
	{
		const int numThreads = blockDim.x * gridDim.x;
		const int id = blockIdx.x * blockDim.x + threadIdx.x;
		int j=(id%S)*N+(id-id%S)/S;
		for (int i = id; i < N*S; i+=numThreads, j=(i%S)*N+(i-i%S)/S)
		{
			result[i] = init[j] + dt * (smol[j] - result[i]);
			if (result[i] < 0.0) result[i] = 0.0;
			else if (result[i] > 1.0) result[i] = 1.0;
		}
	}

	__global__ void equalize_data(double *init, double *smol, double *result, const int N, const int S)
	{
		const int numThreads = blockDim.x * gridDim.x;
		const int threadID = blockIdx.x * blockDim.x + threadIdx.x;
		for (int i = threadID; i < N*S; i += numThreads)
		{
			init[i] = result[(i%N)*S+(i-i%N)/N];
			smol[i] = result[(i%N)*S+(i-i%N)/N];
		}
	}

	__global__ void new_time_step_aligned(const double *init, const double *smol, double *result, const int N, const int S, const double dt)
	{
		const int numThreads = blockDim.x * gridDim.x;
		const int id = blockIdx.x * blockDim.x + threadIdx.x;
		for (int i = id; i < N*S; i+=numThreads)
		{
			result[i] = init[i] + dt * (smol[i] - result[i]);
			if (result[i] < 0.0) result[i] = 0.0;
			else if (result[i] > 1.0) result[i] = 1.0;
		}
	}

	__global__ void equalize_data_aligned(double *init, double *smol, double *result, const int N, const int S)
	{
		const int numThreads = blockDim.x * gridDim.x;
		const int threadID = blockIdx.x * blockDim.x + threadIdx.x;
		for (int i = threadID; i < N*S; i += numThreads)
		{
			init[i] = result[i];
			smol[i] = result[i];
		}
	}

	__global__ void calc_smoluch(const double *L1, const double *L2, const double *n_k, double * result, const int N)
	{
		const int numThreads = blockDim.x * gridDim.x;
		const int threadID = blockIdx.x * blockDim.x + threadIdx.x;
		for (int i = threadID; i < N; i += numThreads)
		{
			result[i] = ( L1[i] * 0.5 - n_k[i] * L2[i] );
        	        if (result[i] < 0.0) result[i] = 0.0;
		}
	}

	__global__ void calc_L2_correction(const double * n_k, const double h, double * result, const int N)
	{
		const int numThreads = blockDim.x * gridDim.x;
		const int threadID = blockIdx.x * blockDim.x + threadIdx.x;
		double coef1 = n_k[0];
		double coef2 = n_k[N-1];
		for (int i = threadID; i < N; i += numThreads)
		{
			result[i] = ( K( i, 1, h ) * coef1 + K(i, N, h) * coef2) * h;
		}
	}
}

